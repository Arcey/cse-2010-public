/*
Author: Vincent Quintero
Email: 
Course: CSE 2010
Section: 02
Description of this file: Logic to manage a heirarchy of entities
*/

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class HW3 {
    public static void main(final String[] args) throws FileNotFoundException {
        final Tree<String> tree = parseEntitiesToTree(new File(args[0])); // arg[0] is entities file
        performQueries(new File(args[1]), tree);// arg[1] is queries file
    }

    private static Tree<String> parseEntitiesToTree(final File f) throws FileNotFoundException {
        final Tree<String> tree = new Tree<String>();
        final Scanner scanner = new Scanner(f);

        // Get the root node before reading child nodes
        tokensToNodes(scanner.nextLine(), tree);

        // Read in all remaining entities from the given file
        while (scanner.hasNextLine()) {
            final String line = scanner.nextLine();
            tokensToNodes(line, tree);
        }

        scanner.close();
        return tree;
    }

    /**
     * Parse the given file of queries and execute them against the data in the
     * given tree.
     * 
     * @param f    file to parse
     * @param tree tree to store contents of parsed file in
     * @throws FileNotFoundException file does not exist, cannot read
     */
    private static void performQueries(final File f, final Tree<String> tree) throws FileNotFoundException {
        final Scanner scanner = new Scanner(f);

        while (scanner.hasNextLine()) {
            final String[] tokens = scanner.nextLine().split(" ");
            handleQuery(tokens, tree);
        }

        scanner.close();
    }

    /**
     * Combine separated words into one string with whitespace delimiters
     * 
     * @param words list of words (in order) composing the phrase
     * @return whitespace separated string of all words in given phrase
     */
    private static String stringify(final String[] words) {
        final StringBuilder sb = new StringBuilder();

        for (int i = 0; i < words.length; i++) {
            sb.append(words[i]);

            // Only add a space if another word is going to be added
            if (i != words.length - 1) {
                sb.append(' ');
            }
        }

        return sb.toString();
    }

    /**
     * Convenience method take the value of every child and place it into an array
     * to make it stringify-friendly
     * 
     * @param nodes to be stringified
     * @return list of stringified node datapoints
     */
    private static String[] nodesToStrings(List<Node<String>> nodes) {
        // Take the value of every child and place it into an array
        return nodes.stream().map(e -> e.data).toArray(s -> new String[s]);
    }

    /**
     * Takes a given string of tokens and splits them apart. It then
     * turns each token into a node and places it in a specified position
     * 
     * @param line String of tokens to split and parse into nodes
     * @param tree Tree the nodes will be placed into
     */
    private static void tokensToNodes(final String line, final Tree<String> tree) {
        final String[] tokens = line.split(" ");

        // Find the location of the first token in the tree, create if nonexistent
        final Node<String> parent = tree.getNode(tokens[0], tree.root());
        if (parent == null) {
            tree.addNode(tokens[0], tree.root());
        }

        // Skip index 0 because that is the parent node for all subsequent tokens
        for (int i = 1; i < tokens.length; i++) {
            tree.addNode(tokens[i], parent);
        }
    }

    /**
     * Convenience method for printQuery()
     * 
     * @param query contents to be printed
     */
    private static void printQuery(final String... query) {
        printQuery(null, query);
    }

    /**
     * Print out all of the args provided in query with proper spacing. Then any
     * nodes.
     * 
     * @param query contents to be printed
     * @param nodes result of query to be printed if applicable
     */
    private static void printQuery(final List<Node<String>> nodes, final String... query) {
        for (int i = 0; i < query.length; i++) {
            if (query[i] == null) {
                continue;
            }

            System.out.printf("%s", query[i]);
            if (i != query.length - 1) {
                System.out.printf(" ");
            }
        }

        if (nodes != null) {
            if (nodes != null) {
                System.out.printf(" %s", stringify(nodesToStrings(nodes)));
            }
        }

        System.out.println("");
    }

    /**
     * Performs the tokenized query against the data in the given tree
     * 
     * @param tokens data necessary to perform requested query
     * @param tree   object to query against
     */
    private static void handleQuery(final String[] tokens, final Tree<String> tree) {
        // tokens[0] is always the query instruction
        switch (tokens[0]) {
            case QUERY_DIRECTSUPERVISOR:
                printQuery(QUERY_DIRECTSUPERVISOR, tokens[1],
                        handleDirectSupervisor(tokens[1], tree).data);
                break;
            case QUERY_DIRECTSUBORDINATES:
                printQuery(handleDirectSubordinates(tokens[1], tree),
                        QUERY_DIRECTSUBORDINATES, tokens[1]);
                break;
            case QUERY_ALLSUPERVISORS:
                printQuery(handleAllSupervisors(tokens[1], tree),
                        QUERY_ALLSUPERVISORS, tokens[1]);
                break;
            case QUERY_ALLSUBORDINATES:
                printQuery(handleAllSubordinates(tokens[1], tree),
                        QUERY_ALLSUBORDINATES, tokens[1]);
                break;
            case QUERY_NUMBEROFALLSUPERVISORS:
                printQuery(QUERY_NUMBEROFALLSUPERVISORS, tokens[1],
                        String.valueOf(handleNumberOfAllSupervisors(tokens[1], tree)));
                break;
            case QUERY_NUMBEROFALLSUBORDINATES:
                printQuery(QUERY_NUMBEROFALLSUBORDINATES, tokens[1],
                        String.valueOf(handleNumberOfAllSubordinates(tokens[1], tree)));
                break;
            case QUERY_ISSUPERVISOR:
                printQuery(QUERY_ISSUPERVISOR, tokens[1], tokens[2],
                        handleIsSupervisor(tokens[1], tokens[2], tree) ? TRUE : FALSE);
                break;
            case QUERY_ISSUBORDINATE:
                printQuery(QUERY_ISSUBORDINATE, tokens[1], tokens[2],
                        handleIsSubordinate(tokens[1], tokens[2], tree) ? TRUE : FALSE);
                break;
            case QUERY_COMPARERANK:
                printQuery(QUERY_COMPARERANK, tokens[1], tokens[2],
                        handleCompareRank(tokens[1], tokens[2], tree));
                break;
            case QUERY_CLOSESTCOMMONSUPERVISOR:
                printQuery(QUERY_CLOSESTCOMMONSUPERVISOR, tokens[1], tokens[2],
                        handleClosestCommonSupervisor(tokens[1], tokens[2], tree).data);
                break;
            default:
                break;
        }
    }

    /**
     * Get the parent of the specified entity
     * 
     * @param entity specific entity to find in the tree
     * @param tree   object containing all entities
     * @return the parent node of the specified entity or null if none
     */
    private static Node<String> handleDirectSupervisor(final String entity, final Tree<String> tree) {
        // Find the entity in the tree (it should be guaranteed to exist)
        final Node<String> node = tree.getNode(entity, tree.root());

        if (node != null) {
            return node.parent;
        }
        return null;
    }

    /**
     * Gets the children of the specified entity
     * 
     * @param entity specific entity to find in the tree
     * @param tree   object containing all entities
     * @return an array of child nodes of the specified entity or null if none
     */
    private static List<Node<String>> handleDirectSubordinates(final String entity, final Tree<String> tree) {
        // Find the entity in the tree (it should be guaranteed to exist)
        final Node<String> node = tree.getNode(entity, tree.root());

        if (node != null && !node.children.isEmpty()) {
            return node.children;
        }
        return null;
    }

    /**
     * Get all parents of the specified entity
     * 
     * @param entity specific entity to find in the tree
     * @param tree   object containing all entities
     * @return the parents of the specified entity or null if none
     */
    private static List<Node<String>> handleAllSupervisors(final String entity, final Tree<String> tree) {
        Node<String> node = tree.getNode(entity, tree.root());
        final LinkedList<Node<String>> nodes = new LinkedList<>();

        // Traverse upwards until there are no more parents
        while (node.parent != null) {
            node = node.parent;
            nodes.add(node);
        }

        return nodes;
    }

    /**
     * Get all children of the specified entity
     * 
     * @param entity specific entity to find in the tree
     * @param tree   object containing all entities
     * @return the children of the specified entity or null if none
     */
    private static List<Node<String>> handleAllSubordinates(final String entity, final Tree<String> tree) {
        // Get the children of the entity to determine where to start searching
        final List<Node<String>> roots = handleDirectSubordinates(entity, tree);

        if (roots == null) {
            return null;
        }

        final List<Node<String>> nodes = new LinkedList<>(); // Store all subordinates

        // Traverse the tree in preorder fashion for each direct subordinate
        for (final Node<String> node : roots) {
            tree.preorder(node, n -> nodes.add(n));
        }

        return nodes;
    }

    /**
     * Get the number of parents of the specified entity
     * 
     * @param entity specific entity to find in the tree
     * @param tree   object containing all entities
     * @return the number of parent nodes of the specified entity
     */
    private static int handleNumberOfAllSupervisors(final String entity, final Tree<String> tree) {
        // This is actually just the depth of the entity given
        final List<Node<String>> nodes = handleAllSupervisors(entity, tree);
        return nodes != null ? nodes.size() : 0;
    }

    /**
     * Get the number of children of the specified entity
     * 
     * @param entity specific entity to find in the tree
     * @param tree   object containing all entities
     * @return the number of children nodes of the specified entity
     */
    private static int handleNumberOfAllSubordinates(final String entity, final Tree<String> tree) {
        final List<Node<String>> nodes = handleAllSubordinates(entity, tree);
        return nodes != null ? nodes.size() : 0;
    }

    /**
     * Prints yes/no depending on whether or not the supervisor is the parent of
     * entity. Helps to think "Is [supervisor] a parent of [entity]?"
     * 
     * @param entity     potential child of the supervisor
     * @param supervisor potential parent of the entity
     * @param tree       object containing all entities
     */
    private static boolean handleIsSupervisor(final String entity, final String supervisor, final Tree<String> tree) {
        // Find the entity in the tree (it should be guaranteed to exist)
        Node<String> node = tree.getNode(entity, tree.root());

        // Just iterate up each node's parent until a parent == supervisor or there are
        // no more parents
        while (node != null) {
            if (node.data.equals(supervisor)) {
                return true;
            }
            node = node.parent; // Move up to check the next node
        }
        return false;
    }

    /**
     * Prints yes/no depending on whether or not the subordinate is the child of
     * entity. Helps to think "Is [subordinate] a child of [entity]?"
     * 
     * @param entity      potential parent of the subordinate
     * @param subordinate potential child of the entity
     * @param tree        object containing all entities
     */
    private static boolean handleIsSubordinate(final String entity, final String subordinate,
            final Tree<String> tree) {
        // Find the entity in the tree (it should be guaranteed to exist)
        Node<String> node = tree.getNode(subordinate, tree.root());

        // Just iterate up each node's parent until a parent == supervisor or there are
        // no more parents
        while (node != null) {
            if (node.data.equals(entity)) {
                return true;
            }
            node = node.parent; // Move up to check the next node
        }
        return false;
    }

    /**
     * Determine if the first entity is deeper in the tree than the second entity.
     * 
     * @param entity  to check depth of
     * @param entity2 to compare depth against
     * @param tree    object containing all entities
     * @return whether the entity is "higher", "lower", or the "same" depth
     */
    private static String handleCompareRank(final String entity, final String entity2, final Tree<String> tree) {
        final int depth = tree.getDepth(tree.getNode(entity2, tree.root()))
                - tree.getDepth(tree.getNode(entity, tree.root()));

        // If entity1 is deeper than entity2, depth is negative
        // If entity2 is deeper than entity1, depth is positive
        if (depth > 0) {
            return RANK_HIGHER;
        } else if (depth < 0) {
            return RANK_LOWER;
        }
        return RANK_SAME;
    }

    /**
     * Determine the closest shared parent for both entities
     * 
     * @param entity  to find shraed parent of
     * @param entity2 to find shraed parent of
     * @param tree    object containing all entities
     * @return the closest shared parent or null if none
     */
    private static Node<String> handleClosestCommonSupervisor(final String entity, final String entity2,
            final Tree<String> tree) {
        // Get all supervisors of both entites, then reduce the list to the first common
        // parent
        final List<Node<String>> parents = handleAllSupervisors(entity, tree);
        final List<Node<String>> parents2 = handleAllSupervisors(entity2, tree);

        // There are no common supervisors (The root was passed in as an entity)
        if (parents.isEmpty() || parents2.isEmpty()) {
            return null;
        }

        // Remove all Nodes that are not in both lists
        parents.retainAll(parents2);

        // First element should be the closest common supervisor
        return parents.get(0);
    }

    static final String QUERY_DIRECTSUPERVISOR = "DirectSupervisor";
    static final String QUERY_DIRECTSUBORDINATES = "DirectSubordinates";
    static final String QUERY_ALLSUPERVISORS = "AllSupervisors";
    static final String QUERY_ALLSUBORDINATES = "AllSubordinates";
    static final String QUERY_NUMBEROFALLSUPERVISORS = "NumberOfAllSupervisors";
    static final String QUERY_NUMBEROFALLSUBORDINATES = "NumberOfAllSubordinates";
    static final String QUERY_ISSUPERVISOR = "IsSupervisor";
    static final String QUERY_ISSUBORDINATE = "IsSubordinate";
    static final String QUERY_COMPARERANK = "CompareRank";
    static final String QUERY_CLOSESTCOMMONSUPERVISOR = "ClosestCommonSupervisor";
    static final String TRUE = "yes";
    static final String FALSE = "no";
    static final String RANK_HIGHER = "higher";
    static final String RANK_SAME = "same";
    static final String RANK_LOWER = "lower";
}
