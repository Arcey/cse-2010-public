/*
Author: Vincent Quintero
Email: 
Course: CSE 2010
Section: 02
Description of this file: Data Structure representing a Node to be used in a Tree
*/

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Node<T extends Comparable<T>> implements Comparable<Node<T>> {
    public Node<T> parent;
    public T data;
    public List<Node<T>> children;

    public Node (T data, Node<T> parent) {
        this.data = data;
        this.parent = parent;
        this.children = new LinkedList<Node<T>>();
    }

    /**
     * Add a new node to this node's list of children
     * 
     * @param child node to add to this node's list of children
     * @return the child node added to the list
     */
    public Node<T> addChild (final Node<T> child) {
        this.children.add(child);
        Collections.sort(this.children);
        return child;
    }

    @Override
    public String toString () {
        return data.toString();
    }

    @Override
    public int compareTo(final Node<T> node) {
        return this.data.compareTo(node.data);
    }
}
