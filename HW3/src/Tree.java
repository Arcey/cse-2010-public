import java.util.Queue;
import java.util.function.Consumer;
import java.util.LinkedList;

/*
Author: Vincent Quintero
Email: 
Course: CSE 2010
Section: 02
Description of this file: Data structure representing a Tree
*/

public class Tree<T extends Comparable<T>> {

    private Node<T> root = null;
    private int size = 0; // Number of nodes in the tree

    public Tree() {}

    public Node<T> root() {
        return this.root;
    }

    public boolean isEmpty() {
        return this.size() == 0;
    }

    public int size() {
        return this.size;
    }

    /** 
     * Add a node with data T and given parent to tree
     * 
     * @param child data to populate new node with
     * @param parent parent of new node
     * @return the created node
    */
    public Node<T> addNode(final T child, final Node<T> parent) {
        final Node<T> newNode;
        if (this.root == null) {
            newNode = new Node<T>(child, null);
            this.root = newNode;
        } else {
            newNode = new Node<T>(child, parent);
            parent.addChild(newNode);
        }
        this.size++;
        return newNode;
    }

    /**
     * Locates a node containing the given element if one exists in the tree via BFS.
     * 
     * @param element the element to locate in the tree
     * @param node where to begin searching in the tree
     * @return the node containing the given element if it exists, otherwise null
     */
    public Node<T> getNode(final T element, final Node<T> node) {
        if (node == null) {
            return null;
        }

        final Queue<Node<T>> toCheck = new LinkedList<Node<T>>();
        toCheck.add(node);

        while (!toCheck.isEmpty()) {
            final Node<T> current = toCheck.poll();
            if (current.data.equals(element)) {
                return current;
            }

            for (final Node<T> child : current.children) {
                toCheck.add(child);
            }
        }

        return null;
    }

    /**
     * Find the depth of the given node in the tree
     * 
     * @param node to find depth of
     * @return depth of the given node
     */
    public int getDepth(Node<T> node) {
        if (node == this.root) {
            return 0;
        }
        return 1 + getDepth(node.parent);
    }

    /**
     * Visit and print out each node in the tree, going from left to right visiting
     * parents before children.
     * 
     * @param node to begin traversing the tree at
     * @param visit callback function that will be called whenever a node should be visited
     */
    public void preorder(Node<T> node, Consumer<Node<T>> visit) {
        visit.accept(node);
        for (final Node<T> n : node.children) {
            preorder(n, visit);
        }
    }
}
