# CSE 2010 Public
Public repo containing all work done in CSE 2010, Algorithms & Data Structures, with sensitive info removed

### HW1
Program to simulate an online retailer provides a service that allows chatting on their website with a customer service representative.

### HW2
Program to find multiword palindromes in a list of words.

### HW3
Program to build a tree from organizational data and answer queries on the organizational structure.

### HW4
Program to recommend music for a customer based on another customer with the most similar taste.

### HW5
Program to manage the timeline of a user's social media profile and allow the user to specify a time range to display/share his/her major events.

### HW6
Program to simulate a 2D game of Humans vs Zombies. Given a starting position in a 2D grid world,
a player’s goal is to reach the destination (e.g., home sweet
home) and avoid stationary obstacles (e.g., buildings, garbage
dumpster, and lamp posts (ouch!)) and undesirable moving
creatures (e.g., zombies, alligators, bears (in Florida? yes),
and barking dogs).
