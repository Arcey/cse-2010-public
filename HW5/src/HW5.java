/*
Author: Vincent Quintero
Email: 
Course: CSE 2010
Section: 02
Description of this file: Logic to manage a timeline
*/

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;

public class HW5 {
    public static void main(final String[] args) throws FileNotFoundException {
        final Scanner scanner = new Scanner(new File(args[0]));

        // Must set the date format that will be used to read in dates
        SimpleDate.setFormat(INF_DATE_FORMAT);
        final SkipList<SimpleDate, String> skipList = initializeSkipList(new SimpleDate(NEG_INF_DATE),
                new SimpleDate(POS_INF_DATE));

        SimpleDate.setFormat(DATE_FORMAT);
        // Read in all remaining entities from the given file
        while (scanner.hasNextLine()) {
            final String[] tokens = scanner.nextLine().split(" ");
            System.out.println(handleAction(tokens, skipList));
        }

        scanner.close();
    }

    /**
     * Create a new SkipList with the given dates as pos/neg infinities
     * 
     * @param min negative infinity
     * @param max positive infinity
     * @return new skiplist
     */
    private static SkipList<SimpleDate, String> initializeSkipList(SimpleDate min, SimpleDate max) {
        return new SkipList<SimpleDate, String>(min, max);
    }

    /**
     * Stringify a given map's keys/values in the assignments required format
     * 
     * @param map to be stringified
     * @return String of map's keys/values in the output format desired
     */
    private static String mapToString(Map<SimpleDate, String> map) {
        final StringBuilder sb = new StringBuilder();

        int count = 0;
        for (Map.Entry<SimpleDate, String> entry : map.entrySet()) {
            sb.append(entry.getKey()).append(":").append(entry.getValue());
            count++;
            if (count != map.size()) {
                sb.append(" ");
            }
        }

        return sb.toString();
    }

    /**
     * Create a string composed of the given args, separated by a delimiter
     * (delimiter will not trail)
     * 
     * @param delimiter separator between given args
     * @param args      Strings to be separated by delimiter
     * @return String composed of the given args separated by delimiter
     */
    private static String buildDelimitedString(String delimiter, String... args) {
        final StringBuilder sb = new StringBuilder();

        for (int i = 0; i < args.length; i++) {
            sb.append(args[i]);
            if (i != args.length - 1) {
                sb.append(delimiter);
            }
        }
        return sb.toString();
    }

    /**
     * Parses tokens given and determines the type of action to take
     * 
     * @param tokens to parse dictating the action to take
     */
    private static String handleAction(final String[] tokens, final SkipList<SimpleDate, String> skipList) {
        final StringBuilder sb = new StringBuilder();
        // Tokens[0] always action
        switch (tokens[0]) {
            case ACTION_DISPLAYEVENT:
                sb.append(handleDisplayEvent(tokens, skipList));
                break;
            case ACTION_ADDEVENT:
                sb.append(handleAddEvent(tokens, skipList));
                break;
            case ACTION_DELETEEVENT:
                sb.append(handleDeleteEvent(tokens, skipList));
                break;
            case ACTION_DISPLAYEVENTSBETWEENDATES:
                sb.append(handleDisplayBetween(tokens, skipList));
                break;
            case ACTION_DISPLAYEVENTSFROMSTARTDATE:
                sb.append(handleDisplayFrom(tokens, skipList));
                break;
            case ACTION_DISPLAYEVENTSTOENDDATE:
                sb.append(handleDisplayTo(tokens, skipList));
                break;
            case ACTION_DISPLAYALLEVENTS:
                sb.append(handleDisplayAll(tokens, skipList));
                break;
            case ACTION_PRINTSKIPLIST:
                sb.append(ACTION_PRINTSKIPLIST).append("\n").append(skipList);
                break;
            default:
                break;
        }
        // sb.append("\n");

        return sb.toString();
    }

    /**
     * Get an event matching the given key if it exists
     * 
     * @param tokens   containing the event key to search for
     * @param skipList to search for the key in
     * @return String describing the success or failure of the search
     */
    private static String handleDisplayEvent(String[] tokens, SkipList<SimpleDate, String> skipList) {
        final String value = skipList.get(new SimpleDate(tokens[1]));
        return buildDelimitedString(OUTPUT_DELIMITER, ACTION_DISPLAYEVENT, tokens[1],
                value != null ? value : EVENT_DOES_NOT_EXIST);
    }

    /**
     * Create an event with the given key and value
     * 
     * @param tokens   containing the event key and value to create
     * @param skipList to place the event in
     * @return String describing the success or failure the addition
     */
    private static String handleAddEvent(String[] tokens, SkipList<SimpleDate, String> skipList) {
        final boolean result = skipList.put(new SimpleDate(tokens[1]), tokens[2]);
        return buildDelimitedString(OUTPUT_DELIMITER, ACTION_ADDEVENT, tokens[1], tokens[2],
                result ? ACTION_SUCCEEDED : ACTION_FAILED);
    }

    /**
     * Delete an event matching the given key if it exists
     * 
     * @param tokens   containing the event key to delete
     * @param skipList to search for the event in
     * @return String describing the success or failure of the deletion
     */
    private static String handleDeleteEvent(String[] tokens, SkipList<SimpleDate, String> skipList) {
        final boolean result = skipList.remove(new SimpleDate(tokens[1])) != null;
        return buildDelimitedString(OUTPUT_DELIMITER, ACTION_DELETEEVENT, tokens[1],
                result ? ACTION_SUCCEEDED : DATE_DOES_NOT_EXIST);
    }

    /**
     * Get all events between two time periods (inclusive)
     * 
     * @param tokens   containing the event keys to search between
     * @param skipList to search for the events in
     * @return String describing the events between the periods or lack thereof
     */
    private static Object handleDisplayBetween(String[] tokens, SkipList<SimpleDate, String> skipList) {
        final Map<SimpleDate, String> entries = skipList.subMap(new SimpleDate(tokens[1]), new SimpleDate(tokens[2]));
        if (entries.size() > 0) {
            return buildDelimitedString(OUTPUT_DELIMITER, ACTION_DISPLAYEVENTSBETWEENDATES, tokens[1], tokens[2],
                    mapToString(entries));
        }
        return buildDelimitedString(OUTPUT_DELIMITER, ACTION_DISPLAYEVENTSBETWEENDATES, tokens[1], tokens[2],
                EVENT_DOES_NOT_EXIST);
    }

    /**
     * Get all events from a time period to the end of the year (inclusive)
     * 
     * @param tokens   containing the event key to start from
     * @param skipList to search for the events in
     * @return String describing the events on or after the given key, or lack
     *         thereof
     */
    private static Object handleDisplayFrom(String[] tokens, SkipList<SimpleDate, String> skipList) {
        final Map<SimpleDate, String> entries = skipList.subMap(new SimpleDate(tokens[1]),
                new SimpleDate(MAX_DATE_SEARCHABLE));
        if (entries.size() > 0) {
            return buildDelimitedString(OUTPUT_DELIMITER, ACTION_DISPLAYEVENTSFROMSTARTDATE, tokens[1],
                    mapToString(entries));
        }
        return buildDelimitedString(OUTPUT_DELIMITER, ACTION_DISPLAYEVENTSFROMSTARTDATE, tokens[1],
                EVENT_DOES_NOT_EXIST);
    }

    /**
     * Get all events from a given time period to the end of the year (inclusive)
     * 
     * @param tokens   containing the event key to stop at
     * @param skipList to search for the events in
     * @return String describing the events up to and including the given key, or
     *         lack thereof
     */
    private static Object handleDisplayTo(String[] tokens, SkipList<SimpleDate, String> skipList) {
        final Map<SimpleDate, String> entries = skipList.subMap(new SimpleDate(MIN_DATE_SEARCHABLE),
                new SimpleDate(tokens[1]));
        if (entries.size() > 0) {
            return buildDelimitedString(OUTPUT_DELIMITER, ACTION_DISPLAYEVENTSTOENDDATE, tokens[1],
                    mapToString(entries));
        }
        return buildDelimitedString(OUTPUT_DELIMITER, ACTION_DISPLAYEVENTSTOENDDATE, tokens[1], EVENT_DOES_NOT_EXIST);
    }

    /**
     * Get all events in the year
     * 
     * @param tokens   containing any special requirements
     * @param skipList to get all events from
     * @return String describing all events in the year, or lack thereof
     */
    private static Object handleDisplayAll(String[] tokens, SkipList<SimpleDate, String> skipList) {
        final Map<SimpleDate, String> entries = skipList.subMap(new SimpleDate(MIN_DATE_SEARCHABLE),
                new SimpleDate(MAX_DATE_SEARCHABLE));
        if (entries.size() > 0) {
            return buildDelimitedString(OUTPUT_DELIMITER, ACTION_DISPLAYALLEVENTS, mapToString(entries));
        }
        return buildDelimitedString(OUTPUT_DELIMITER, ACTION_DISPLAYALLEVENTS, EVENT_DOES_NOT_EXIST);
    }

    private static final String NEG_INF_DATE = "00000101";
    private static final String POS_INF_DATE = "99991231";
    private static final String MIN_DATE_SEARCHABLE = "0101"; // Date that can be used to find the highest finite key
    private static final String MAX_DATE_SEARCHABLE = "1231"; // Date that can be used to find the lowest finite key
    private static final String DATE_FORMAT = "MMdd";
    private static final String INF_DATE_FORMAT = "yyyyMMdd";
    private static final String EVENT_DOES_NOT_EXIST = "none";
    private static final String ACTION_SUCCEEDED = "success";
    private static final String ACTION_FAILED = "failure";
    private static final String DATE_DOES_NOT_EXIST = "noDateError";
    private static final String OUTPUT_DELIMITER = " ";

    // Valid input actions
    private static final String ACTION_DISPLAYEVENT = "DisplayEvent";
    private static final String ACTION_ADDEVENT = "AddEvent";
    private static final String ACTION_DELETEEVENT = "DeleteEvent";
    private static final String ACTION_DISPLAYEVENTSBETWEENDATES = "DisplayEventsBetweenDates";
    private static final String ACTION_DISPLAYEVENTSFROMSTARTDATE = "DisplayEventsFromStartDate";
    private static final String ACTION_DISPLAYEVENTSTOENDDATE = "DisplayEventsToEndDate";
    private static final String ACTION_DISPLAYALLEVENTS = "DisplayAllEvents";
    private static final String ACTION_PRINTSKIPLIST = "PrintSkipList";
}
