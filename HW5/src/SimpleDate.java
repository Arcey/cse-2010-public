/*
Author: Vincent Quintero
Email: 
Course: CSE 2010
Section: 02
Description of this file: 
*/

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Simple class to allow for easy date creation, formatting, and parsing.
 */
public class SimpleDate implements Comparable<SimpleDate> {
    private static final SimpleDateFormat formatter = new SimpleDateFormat();
    private Date date;

    /**
     * Create a new Date with the given String in the specified format
     * 
     * @param d      date
     * @param format of the given date
     */
    public SimpleDate(String d, String format) {
        SimpleDate.setFormat(format);
        this.date = SimpleDate.parseDate(d);
    }

    /**
     * Create a new Date with the given String in the class' format
     * 
     * @param d date
     */
    public SimpleDate(String d) {
        this(d, formatter.toPattern());
    }

    /**
     * Update the default format the class reads dates in
     * 
     * @param pattern new date format
     */
    public static void setFormat(String pattern) {
        formatter.applyPattern(pattern);
    }

    /**
     * Turn a given date String into a date object
     * 
     * @param date to be turned into a date object
     * @return Date object version of the given String
     */
    public static Date parseDate(String date) {
        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * Get this instance's date
     * 
     * @return this instance's date
     */
    public Date getDate() {
        return this.date;
    }

    /**
     * Set this instance's date
     * 
     * @param date this instance's new date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Set this instance's date
     * 
     * @param date this instance's new date
     */
    public void setDate(String date) {
        this.date = SimpleDate.parseDate(date);
    }

    /**
     * Represent this instance as a String
     * 
     * @return this instance as a String
     */
    @Override
    public String toString() {
        return formatter.format(this.date);
    }

    /**
     * Compares this date with the given date
     * 
     * @param o the object to compare against
     * @return a negative integer, zero, or a positive integer as this object is
     *         less than, equal to, or greater than the specified object.
     */
    @Override
    public int compareTo(SimpleDate o) {
        return this.date.compareTo(o.getDate());
    }

}
