/*
Author: Vincent Quintero
Email: 
Course: CSE 2010
Section: 02
Description of this file: 
*/
import java.util.TreeMap;

// Ordered from most to least populated, so lowest index should always be full
// Highest index should always be just the min and max keys
public class SkipList<K extends Comparable<K>, E extends Comparable<E>> {
    private final DoublyLinkedList<QuadNode<K, E>> skipList;
    private final FakeRandHeight rng; // The H/T coinflip to determine insertion height
    private final K maxKey; // Key representing where a skipList ends
    private final K minKey; // Key representing where a skipList begins

    /**
     * Initialize a new skipList with the given infinities
     * 
     * @param minKey negative infinity
     * @param maxKey positive infinity
     */
    public SkipList(K minKey, K maxKey) {
        this.minKey = minKey;
        this.maxKey = maxKey;
        this.skipList = new DoublyLinkedList<>();
        this.rng = new FakeRandHeight();
        addLevel();
    }

    /**
     * Creates a new empty level in the with just min and max keys
     */
    private void addLevel() {
        // Add 1 to height to account for the new level they will be added to
        final QuadNode<K, E> min = new QuadNode<>(minKey, null, height());
        final QuadNode<K, E> max = new QuadNode<>(maxKey, null, height());

        linkVertically(min, head()); // Link minKeys
        linkVertically(max, tail()); // Link maxKeys

        this.skipList.addLast(min);
        insert(max, min);
    }

    /**
     * Inserts a node "horizontally" in the linked list after given before node
     * The given nodes are expected to be at the same height
     * 
     * @param node   new node to be inserted
     * @param before the node that will be before the new node
     */
    private void insert(QuadNode<K, E> node, QuadNode<K, E> before) {
        if (before.getNext() != null) {
            node.setNext(before.getNext());
            node.getNext().setPrev(node);
        }
        node.setPrev(before);
        before.setNext(node);
    }

    /**
     * Links the two given nodes vertically
     * 
     * @param top    node to be linked
     * @param bottom node to be linked
     */
    private void linkVertically(QuadNode<K, E> top, QuadNode<K, E> bottom) {
        if (top != null)
            top.setBottom(bottom);
        if (bottom != null)
            bottom.setTop(top);
    }

    /**
     * Horizontally deletes the given node, removing its prev and next references
     * Vertical references stay intact
     * 
     * @param node to be horizontally deleted
     */
    private void deleteHorizontally(QuadNode<K, E> node) {
        node.getPrev().setNext(node.getNext());
        node.getNext().setPrev(node.getPrev());
    }

    /**
     * Convenience method to determine if two keys are the same
     * 
     * @param k1 first key to compare
     * @param k2 second key to compare against
     * @return true if keys are same, false otherwise
     */
    private boolean sameKey(K k1, K k2) {
        return k1.compareTo(k2) == 0;
    }

    /**
     * Determine if the level the given node is at is empty or not
     * 
     * @param n node to get level from
     * @return true if the only keys in the level are min & max or if n is null,
     *         false otherwise
     */
    private boolean isEmptyLevel(QuadNode<K, E> n) {
        return n != null ? sameKey(n.getKey(), minKey) && sameKey(n.getNext().getKey(), maxKey) : true;
    }

    /**
     * Ensures the skipList only ever has one empty list at the top
     */
    private void validateState() {
        // Ensure topmost list is empty
        if (!isEmptyLevel(head())) {
            addLevel();
        }

        // Get level directly below the topmost list that is guaranteed to be empty
        QuadNode<K, E> node = head().getBottom();

        if (node != null) {
            while (isEmptyLevel(node)) {
                node = node.getBottom();
                // If this level is also empty, just remove the topmost empty level
                // and make this empty level the topmost empty level
                removeTopLevel();
            }
        }
    }

    /**
     * Deletes the topmost level from the skipList, usually this level is empty
     * and this method is called because there are multiple empty levels.
     * Ensures all nodes underneath the topmost level have their top references
     * removed
     */
    private void removeTopLevel() {
        // We only ever remove empty levels, and empty levels only exist above filled
        // levels so removing the last level then destroying vertical links works
        QuadNode<K, E> node = this.skipList.removeLast();
        while (node != null && node.getNext() != null) {
            // Link the node above THIS node and the node below THIS node, effectively
            // deleting this node. If either reference is null, that is fine as the
            // top/bottom
            // will just be set to null
            linkVertically(node.getTop(), node.getBottom());
            node = node.getNext();
        }
    }

    /**
     * Locate a position to insert the given node at, starting at the given position
     * This method becomes unreliable is node and start share the same key
     * 
     * @param node  to insert into the skipList
     * @param start node to start at when searching for insertion position
     * @return true if the insertion is successful, false otherwise
     */
    private boolean findAndInsert(QuadNode<K, E> node, QuadNode<K, E> start) {
        // Always runs at least once since even an empty list has a next of maxKey
        while (start.getNext() != null) {
            final int comparison = node.getKey().compareTo(start.getNext().getKey());

            if (comparison < 0) { // next node's key is bigger than current key
                // Insert node in the floor
                insert(node, floor(start));
                // Insert vertically random amount
                insertVertically(node, rng.get());
                return true;
            } else if (comparison > 0) { // next node's key is smaller than current key
                // Continue searching the insertion point
                start = start.getNext();
            }
        }

        // If this happens, something has gone catastrophically wrong
        return false;
    }

    /**
     * Finds the node with the closest available key to the given key while not
     * exceeding the given key
     * 
     * @param key   the key to search up to in the skipList
     * @param floor the lowest level this search should reach
     * @return the node with the closest available key (or the same key) at the
     *         given floor
     */
    private QuadNode<K, E> closestAtOrBelow(K key, int floor) {
        // Perform normal search and just return the closest if a key is not found
        QuadNode<K, E> node = head();

        while (node != null) {
            final int comparison = key.compareTo(node.getNext().getKey());

            if (comparison <= 0) { // Given key is same as or smaller than next key
                if (node.getBottom() != null && node.getLevel() != floor) { // Not desired level
                    node = node.getBottom();
                } else {
                    // Given key is not an exact match to a key in the list, so return current node
                    // Else, given key has an exact match in the list and it is the next node
                    return comparison < 0 ? node : node.getNext();
                }
            } else { // Given key is bigger than the next key
                node = node.getNext();
            }
        }

        return null;
    }

    /**
     * Convenience method to call closestAtOrBelow and search all the way to the
     * floor of the skipList.
     * 
     * @param key to search up to in the skipList
     * @return the node with the closest available key (or same key) at level 0
     */
    private QuadNode<K, E> closestAtOrBelow(K key) {
        return closestAtOrBelow(key, -1);
    }

    /**
     * Finds the node with the closest available key to the given key while not
     * going below the given key
     * 
     * @param key the key to search up to in the skipList
     * @return the node with the closest available key (or the same key) at level 0
     */
    private QuadNode<K, E> closestAtOrAbove(K key) {
        // Start at the tail of the topmost level
        QuadNode<K, E> node = tail();

        while (node != null) {
            final int comparison = key.compareTo(node.getPrev().getKey());

            if (comparison >= 0) { // Given key is same as or bigger than previous key
                if (node.getBottom() != null) { // Not the bottommost node
                    node = node.getBottom();
                } else {
                    // Given key is not an exact match to a key in the list, so return current node
                    // Else, given key has an exact match in the list and it is the prev node
                    return comparison > 0 ? node : node.getPrev();
                }
            } else { // Given key is smaller than the previous key
                node = node.getPrev();
            }
        }

        return null;
    }

    /**
     * Find the node with the given key if it exists in the list
     * 
     * @param key the key of the node to find in the skipList
     * @return the node with the given key at the list's floor or null if it does
     *         not exist
     */
    private QuadNode<K, E> findNode(K key) {
        // Begin searching from the top leftmost node
        QuadNode<K, E> node = head();

        while (node != null) {
            final int comparison = key.compareTo(node.getNext().getKey()); // Is given key >, <, or == current node?

            // Check if the next node is greater than the current key
            if (comparison == 0) { // Same keys
                return floor(node.getNext());
            } else if (comparison > 0) { // Next node's key is smaller than the current key
                // set node to next and continue searching
                node = node.getNext();
            } else { // Key is smaller than the next node
                // reduce level and set node to its bottom
                node = node.getBottom();
            }

        }

        return null;
    }

    /**
     * Replace an existing node's value at every occurrence
     * 
     * @param node    to have the value removed
     * @param element to replace node's value with
     */
    private void overwriteExisting(QuadNode<K, E> node, E element) {
        // Overwrite vertically
        while (node != null) {
            node.setElement(element);
            node = node.getTop();
        }
    }

    /**
     * Inserts a given node from level 1 all the way to the given level.
     * Assumes node already inserted at level 0.
     * 
     * @param node  to be inserted at each level
     * @param level max level this node should be at
     * @return true after node is successfully inserted
     */
    private boolean insertVertically(QuadNode<K, E> node, int level) {
        for (int i = 1; i <= level; i++) {
            if (level >= height()) { // Inserting above last level, create new level
                addLevel();
            }
            final QuadNode<K, E> newNode = new QuadNode<>(node.getKey(), node.getElement(), i, node.getPrev(),
                    node.getNext(), node.getTop(), node.getBottom());
            linkVertically(newNode, node); // Link it vertically to the same node in the list below
            insert(newNode, closestAtOrBelow(node.getKey(), i));
            node = newNode;
        }

        // Ensure there is an empty list at the very top of the skiplist
        validateState();

        return true;
    }

    /**
     * Traverse down this node until reaching the level 0 node
     * 
     * @param node the node to get at level 0
     * @return the given node at level 0
     */
    private QuadNode<K, E> floor(QuadNode<K, E> node) {
        if (node == null) {
            return node;
        }

        while (node.getBottom() != null) {
            node = node.getBottom();
        }
        return node;
    }

    /**
     * @return Get topleftmost node in the skiplist. This is always the negative
     *         infinity in the empty skiplist
     */
    private QuadNode<K, E> head() {
        // Get the list right under the topmost empty list
        return this.skipList.get(height() - 1);
    }

    /**
     * @return Get toprighttmost node in the skiplist. This is always the positive
     *         infinity in the empty skiplist
     */
    private QuadNode<K, E> tail() {
        QuadNode<K, E> node = head();
        if (node == null) {
            return null;
        }

        while (node.getNext() != null) {
            node = node.getNext();
        }
        return node;
    }

    /**
     * Get height of skiplist
     * 
     * @return height of skiplist
     */
    public int height() {
        return skipList.size();
    }

    /**
     * Find and retrieve an item with the given key in the skiplist
     * 
     * @param key to find in skiplist
     * @return element associated with the given key or null if doesn't exist
     */
    public E get(K key) {
        final QuadNode<K, E> node = findNode(key);
        if (node != null) {
            return node.getElement();
        }
        return null;
    }

    /**
     * Insert a new node or overwrite existing node associated with key given
     * 
     * @param key     of the node to be inserted
     * @param element of the node to be inserted
     * @return true on successful insertion
     */
    public boolean put(K key, E element) {
        // Check if node with given key exists
        QuadNode<K, E> givenNode = findNode(key);

        if (givenNode == null) { // This node doesn't exist, create a new one
            givenNode = new QuadNode<K, E>(key, element, 0);
            findAndInsert(givenNode, this.skipList.get(0));
        } else { // This node already exists, overwrite it with new element
            overwriteExisting(givenNode, element);
        }

        // Ensure there is an empty list at the very top of the skiplist
        validateState();

        return true;
    }

    /**
     * Get largest key less than or equal to given key
     * 
     * @param key max key that can be returned
     * @return closest key that is less than or equal to given key or null if none
     */
    public E floorEntry(K key) {
        final QuadNode<K, E> node = closestAtOrBelow(key);
        if (node != null) {
            return node.getElement();
        }
        return null;
    }

    /**
     * Get smallest key greater than or equal to given key
     * 
     * @param key min key that can be returned
     * @return closest key that is greater than or equal to given key or null if
     *         none
     */
    public E ceilingEntry(K key) {
        final QuadNode<K, E> node = closestAtOrAbove(key);
        if (node != null) {
            return node.getElement();
        }
        return null;
    }

    /**
     * Delete the node with the associated key from the skipList
     * 
     * @param key associated with node to delete
     * @return the element of the deleted node
     */
    public E remove(K key) {
        // Use get to find the node with given key
        final QuadNode<K, E> node = findNode(key);

        if (node != null) {
            QuadNode<K, E> tmp = node;

            while (tmp != null) {
                // * Only need to delete the node horizontally to delete it
                // * Since all the vertical links will be deleted with the nodes
                deleteHorizontally(tmp);
                tmp = tmp.getTop();
            }

            validateState();

            return node.getElement();
        }

        validateState();

        // No node with that key exists
        return null;
    }

    /**
     * Get all key/entry pairs in the list between the given keys
     * 
     * @param start key of the first node (inclusive)
     * @param stop  key of the last node (inclusive)
     * @return sorted map containing all key/entry pairs in chronological order
     */
    public TreeMap<K, E> subMap(K start, K stop) {
        final TreeMap<K, E> map = new TreeMap<>();
        QuadNode<K, E> node = closestAtOrAbove(start);
        final QuadNode<K, E> end = closestAtOrBelow(stop);

        // The located start node is larger than the located end node
        if (node.getKey().compareTo(end.getKey()) > 0) {
            return map;
        }

        // Assume stop is ALWAYS in the skipList and ALWAYS after start
        // Go until the nodes key is same as stop's key
        while (node != end) {
            map.put(node.getKey(), node.getElement());
            node = node.getNext();
        }

        // Add in the node with key stop as well
        map.put(node.getKey(), node.getElement());
        return map;
    }

    /**
     * Represent this skipList level by level as a String
     * 
     * @return this skipList as a String
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();

        for (int i = height() - 1; i >= 0; i--) {
            QuadNode<K, E> n = this.skipList.get(i).getNext(); // Get the list of nodes starting after minKey
            sb.append("(S").append(i).append(") ");

            // Call isEmptyLevel on previous node to properly run,
            // since prev is always minKey at this point
            if (isEmptyLevel(n.getPrev())) {
                sb.append("empty");
            } else {
                while (n.getKey() != maxKey) {
                    sb.append(n);
                    if (n.getNext().getKey() != maxKey) {
                        sb.append(" ");
                    }
                    n = n.getNext();
                }

            }
            if (i != 0) {
                sb.append("\n");
            }
        }

        return sb.toString();
    }

    /**
     * Nested QuadNode class to handle 4directions skipList nodes move in
     */
    private class QuadNode<KK, EE> {
        private KK key;
        private int level;

        private EE element;
        private QuadNode<KK, EE> top;
        private QuadNode<KK, EE> prev;
        private QuadNode<KK, EE> next;
        private QuadNode<KK, EE> bottom;

        /**
         * Creates a node with the given values and no references
         * 
         * @param k key of the node
         * @param e element of the node
         * @param l level of the node
         */
        public QuadNode(KK k, EE e, int l) {
            this.key = k;
            this.element = e;
            this.level = l;
            this.top = null;
            this.prev = null;
            this.next = null;
            this.bottom = null;
        }

        /**
         * Creates a node with the given values
         * 
         * @param k key of the node
         * @param e element of the node
         * @param l level of the node
         * @param p reference to the previous node
         * @param n reference to the next node
         * @param t reference to the top node
         * @param b reference to the bottom node
         */
        public QuadNode(KK k, EE e, int l, QuadNode<KK, EE> p, QuadNode<KK, EE> n, QuadNode<KK, EE> t,
                QuadNode<KK, EE> b) {
            this.key = k;
            this.top = t;
            this.prev = p;
            this.next = n;
            this.level = l;
            this.bottom = b;
            this.element = e;
        }

        /**
         * Returns the node that is above this one (or null if none)
         * 
         * @return the above node
         */
        public QuadNode<KK, EE> getTop() {
            return top;
        }

        /**
         * Returns the node that is after this one (or null if none)
         * 
         * @return the next node
         */
        public QuadNode<KK, EE> getNext() {
            return next;
        }

        /**
         * Returns the node that is before this one (or null if none)
         * 
         * @return the previous node
         */
        public QuadNode<KK, EE> getPrev() {
            return prev;
        }

        /**
         * Returns the node that is below this one (or null if none)
         * 
         * @return the below node
         */
        public QuadNode<KK, EE> getBottom() {
            return bottom;
        }

        /**
         * Returns the element of this node
         * 
         * @return element
         */
        public EE getElement() {
            return element;
        }

        /**
         * Returns the level of this node
         * 
         * @return level
         */
        public int getLevel() {
            return level;
        }

        /**
         * Sets the node's top reference
         * 
         * @param top the node that should be above this one
         */
        public void setTop(QuadNode<KK, EE> top) {
            this.top = top;
        }

        /**
         * Sets the node's next reference
         * 
         * @param n the node that should be after this one
         */
        public void setNext(QuadNode<KK, EE> n) {
            this.next = n;
        }

        /**
         * Sets the node's previous reference
         * 
         * @param n the node that should be before this one
         */
        public void setPrev(QuadNode<KK, EE> p) {
            this.prev = p;
        }

        /**
         * Sets the node's bottom reference to point to Node bottom.
         * 
         * @param top the node that should be below this one
         */
        public void setBottom(QuadNode<KK, EE> bottom) {
            this.bottom = bottom;
        }

        /**
         * Sets the node's element
         * 
         * @param element the element that this node should hold
         */
        public void setElement(EE element) {
            this.element = element;
        }

        /**
         * Sets the node's key
         * 
         * @param element the key to associate with this node
         */
        public KK getKey() {
            return key;
        }

        /**
         * Represent this node as a String
         * 
         * @return this node as a String
         */
        @Override
        public String toString() {
            return key + ":" + element;
        }

    }
}
