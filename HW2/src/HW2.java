/*
Author: Vincent Quintero
Email: 
Course: CSE 2010
Section: 02
Description of this file: Logic for creating a list of palindrome phases
*/

//! Do we print nothing if no palindrome phases can be made? Yes, print nothing
// TODO: Are we allowed to use ArrayList
// TODO: Is Regex allowed? (It should be)
// TODO: Will there ever be numbers in the input? (Must not clean numbers from regex if so)

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class HW2 {
    public static void main(final String[] args) throws FileNotFoundException {
        final ArrayList<String> words = new ArrayList<String>();
        final Scanner scanner = new Scanner(new File(args[0])); // arg[0] is filepath
        final int wordsPerPhrase = Integer.parseInt(args[1]);

        // Read in all words from the given file
        while (scanner.hasNext()) {
            words.add(scanner.next());
        }

        // Sort words using String's builtin comparator for alphabetical ordering
        words.sort(String.CASE_INSENSITIVE_ORDER);

        final ArrayList<String> palindromes = new ArrayList<>();
        createPalindromePhrases(wordsPerPhrase, new ArrayList<String>(), words, palindromes);

        palindromes.forEach((s) -> System.out.println(s));
    }

    /**
     * Creates a list of palindrome phrases of a desired length from the given list
     * of words. Phrase is of type ArrayList because it makes swapping out words to
     * test much easier.
     * 
     * @param k                 the number of words that must be added to complete the phrase
     * @param phrase            the phrase created so far
     * @param words             the available list of words to build the phrase from
     * @param palindromePhrases the list that will be updated with valid palindrome phrases
     */
    public static void createPalindromePhrases(final int k, final ArrayList<String> phrase,
            final ArrayList<String> words, final ArrayList<String> palindromePhrases) {

        final ArrayList<String> wordsCopy = new ArrayList<String>();
        wordsCopy.addAll(words);

        for (int i = 0; i < wordsCopy.size(); i++) {
            final String word = wordsCopy.remove(i);
            phrase.add(word);
            
            if (k == 1) { // Cease recursing
                final String finalPhrase = composePhrase(phrase);
                final String cleanedPhrase = cleanString(finalPhrase);

                // The finalPhrase must be cleaned for accurate palindrome test
                if (isPalindrome(cleanedPhrase, 0, cleanedPhrase.length() - 1)) {
                    palindromePhrases.add(finalPhrase);
                }
            } else { // Continue recursing
                createPalindromePhrases(k - 1, phrase, wordsCopy, palindromePhrases);
            }

            phrase.remove(phrase.size() - 1);
            wordsCopy.add(i, word);
        }
    }

    /**
     * Combine the separated words in a phrase into one string with whitespace delimiters
     * 
     * @param phrase list of words (in order) composing the phrase
     * @return whitespace separated string of all words in given phrase
     */
    public static String composePhrase(final ArrayList<String> phrase) {
        final StringBuilder sb = new StringBuilder();

        for (int i = 0; i < phrase.size(); i++) {
            sb.append(phrase.get(i));

            // Only add a space if another word is going to be added
            if (i != phrase.size() - 1) {
                sb.append(' ');
            }
        }

        return sb.toString();
    }

    /**
     * Check if a string is a palindrome by looking at the first and last character.
     * If true remove first & last character, then call again. Repeat until answer
     * is found. Note that an empty string "" IS considered a palindrome, however, given strings
     * are expected to be cleaned before being passed in (no whitespaces/punctuation). This
     * implementation is O(N/2)
     * 
     * @param s    the potential palindrome without any whitespaces or punctuation
     * @param head index of first character that will be checked
     * @param tail index of last character that will be checked
     * @return true if the string is a palindrome, false otherwise
     */
    public static boolean isPalindrome(final String s, final int head, final int tail) {
        // The heard is at/past the tail, meaning every necessary value has been checked
        // Catches if the string is of length 0 or 1 as well, and considers them
        // palindromes
        if (head >= tail) {
            return true;
        }

        if (s.charAt(head) == s.charAt(tail)) {
            // Move to the next pair of characters to see if they're valid
            return isPalindrome(s, head + 1, tail - 1);
        }

        // The two characters did not match
        return false;
    }

    /**
     * Cleans a given string to ensure it is valid for use in isPalindrome.
     * Cleaning removes any invalid characters (whitespaces and punctuation)
     * 
     * @param s dirty string
     * @return cleaned string
     */
    public static String cleanString(final String s) {
        // Regex pattern dictates: Remove ALL characters that are non-alphabetic.
        return s.replaceAll("[^A-Za-z]", "");
    }
}
