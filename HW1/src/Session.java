/*
  Author: Vincent Quintero
  Email: 
  Course: CSE 2010
  Section: 02
  Description of this file: Data class representing a Session between a User and a Rep
 */

public class Session {
    private final User user;
    private final Rep rep;

    public Session(User user, Rep rep) {
        this.user = user;
        this.rep = rep;
    }

    public User getUser() {
        return user;
    }

    public Rep getRep() {
        return rep;
    }
}
