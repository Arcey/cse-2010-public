/*
  Author: Vincent Quintero
  Email: 
  Course: CSE 2010
  Section: 02
  Description of this file: Data class representing a User
 */

public class User {
    private final String name;
    private final int requestHour; // Hour the user connected to the chat service
    private final int requestMinute; // Minute the user connected to the chat service
    private boolean isWaiting = true; // True until the user is assigned a representative
    private Session chatSession = null; // The chat session the user is a part of (if any)

    public User (String name, int requestHour, int requestMinute, boolean isWaiting) {
        this.name = name;
        this.requestHour = requestHour;
        this.requestMinute = requestMinute;
        this.isWaiting = isWaiting;
    }

    public User (String name, int requestHour, int requestMinute) {
        this.name = name;
        this.requestHour = requestHour;
        this.requestMinute = requestMinute;
    }

    public String getName () {
        return name;
    }

    public int getRequestHour () {
        return requestHour;
    }

    public int getRequestMinute () {
        return requestMinute;
    }

    public boolean isWaiting () {
        return isWaiting;
    }

    public void setWaiting (boolean isWaiting) {
        this.isWaiting = isWaiting;
    }

    public Session getChatSession () {
        return chatSession;
    }

    public void setChatSession (Session chatSession) {
        this.chatSession = chatSession;
    }

    /**
     * Calculate the total time the user has been waiting
     * 
     * @param endHour
     *            Hour the user stopped waiting between 0-23
     * @param endMinute
     *            Minute the user stopped waiting between 0-59
     * @return the total time in minutes the user has waited
     */
    public int calculateWaitTime (final int endHour, final int endMinute) {
        // Multiplying the difference between the endHour and startHour by 60 gives
        // the total hours waited in minutes. Ex: endHour=12 - startHour=10 -> 2 hours.
        // 2 hours * 60 = the 120 minutes that have been waited.
        return ((endHour - this.requestHour) * 60) + (endMinute - this.requestMinute);
    }
}
