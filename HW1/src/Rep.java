/*
  Author: Vincent Quintero
  Email: 
  Course: CSE 2010
  Section: 02
  Description of this file: Data class representing a Representative
 */

public class Rep {
   private final String name;
   private boolean waitingForCustomer;
   private Session chatSession;

   public Rep(String name, boolean waitingForCustomer) {
      this.name = name;
      this.waitingForCustomer = waitingForCustomer;
   }

   // Initialize a new Rep that is available for a customer
   public Rep(String name) {
      this.name = name;
      this.waitingForCustomer = true;
   }

   public Session getChatSession() {
      return chatSession;
   }

   public void setChatSession(Session chatSession) {
      this.chatSession = chatSession;
   }

   public String getName() {
      return name;
   }

   public boolean isWaitingForCustomer() {
      return waitingForCustomer;
   }

   public void setWaitingForCustomer(boolean waitingForCustomer) {
      this.waitingForCustomer = waitingForCustomer;
   }

   @Override
   public String toString() {
      return name;
   }
}
