/*
  Author: Vincent Quintero
  Email: 
  Course: CSE 2010
  Section: 02
  Description of this file: Create online chat-queuing service
  to assign waiting customers to a representative when one becomes available
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class HW1 {
    static final String[] reps = { "Alice", "Bob", "Carol", "David", "Emily" };
    static final SinglyLinkedList<Rep> availableReps = new SinglyLinkedList<Rep>();
    static final SinglyLinkedList<User> customersHolding = new SinglyLinkedList<User>();
    static final SinglyLinkedList<Session> chatSessions = new SinglyLinkedList<Session>();

    // Throws FileNotFoundException, this error will not be handled but is necessary
    // for the scanner to work properly.
    public static void main (final String[] args) throws FileNotFoundException {
        final File path = new File(args[0]);
        final Scanner scanner = new Scanner(path);
        int maxWaitTime = 0;

        initializeReps(reps);

        while (scanner.hasNextLine()) {
            final String line = scanner.nextLine();
            final String[] tokens = line.split(" ");

            // The first token is ALWAYS the instruction that must be executed
            switch (tokens[0]) {
            case INST_CHAT_REQ:
                handleChatRequest(tokens);
                break;
            case INST_QUIT_ON_HOLD:
                handleQuitOnHold(tokens);
                break;
            case INST_CHAT_END:
                final int wait = handleChatEnded(tokens);

                if (wait > maxWaitTime)
                    maxWaitTime = wait;

                break;
            case INST_REPS_AVAIL:
                System.out.printf("%s %s%s%n", EVENT_REPS_AVAIL, tokens[1], availableReps.toString());
                break;
            case INST_MAX_WAIT:
                // Nothing should be printed if nobody has waited (wait time = 0)
                final String s = maxWaitTime == 0 ? "" : " " + Integer.toString(maxWaitTime);
                System.out.printf("%s %s%s%n", EVENT_MAX_WAIT, tokens[1], s);
                break;
            default: // Just skip to the next instruction if an invalid one is encountered
                continue;
            }
        }
        scanner.close();
    }

    /**
     * Returns freed up Rep to the availableReps list, deletes the old chatSession,
     * and assigns the freed Rep to another waiting customer if necessary.
     * 
     * @param tokens
     *            List of strings denoting custoemr name, rep name, and time
     * @return the amount of time the a customer waited if a customer was assigned a
     *         Rep, else -1
     */
    private static int handleChatEnded (final String[] tokens) {
        // ? Better implementation would reuse the Rep from the Session instead of
        // creating a new one
        final String userName = tokens[1];
        final String repName = tokens[2];
        final String time = tokens[3];

        // Get the chatSession the rep and user were just in
        final Session endedSession = chatSessions.findFirst(
                chat -> (chat.getUser().getName().equals(userName)
                        && chat.getRep().getName().equals(repName)));

        // Place the Rep back in the pool of availableReps
        availableReps.addLast(endedSession.getRep());

        // Delete the chatSession, and the user with it
        chatSessions.remove(endedSession);

        System.out.printf("%s %s %s %s%n", EVENT_CHAT_END, userName, repName, time);

        // There are customer(s) waiting for a Rep to become available
        if (!customersHolding.isEmpty()) {
            // Assign the the rep at the front of the queue to the customer at the front of
            // the queue (longest waiting)
            final Session session = assignRep(availableReps.removeFirst(),
                    customersHolding.removeFirst(), time);
            final int[] splitTime = parseTime(time);

            chatSessions.addLast(session);

            return session.getUser().calculateWaitTime(splitTime[0], splitTime[1]);
        }
        return -1;
    }

    /**
     * Removes customer from the hold queue
     * 
     * @param tokens
     *            List of strings denoting time and customer name
     */
    private static void handleQuitOnHold (final String[] tokens) {
        final String time = tokens[1];
        final String name = tokens[2];

        // Remove the existing user from the hold line. This marks the user for garbage
        // collection
        customersHolding.remove(customersHolding.findFirst(user -> (name.equals(user.getName()))));

        System.out.printf("%s %s %s%n", EVENT_QUIT_ON_HOLD, time, name);
    }

    /**
     * Assigns a customer to a Rep if one is available, or to the holding queue if
     * not
     * 
     * @param tokens
     *            List of strings denoting custoemr name and time
     */
    private static void handleChatRequest (final String[] tokens) {
        final User user = createUser(tokens);
        final String time = tokens[1];

        System.out.printf("%s %s %s %s%n", EVENT_CHAT_REQ, time, user.getName(),
                user.isWaiting() ? INST_WAIT : INST_LATER);

        // A Rep currently available
        if (!availableReps.isEmpty()) {
            chatSessions.addLast(assignRep(availableReps.removeFirst(), user, time));
        } else {
            // The User will wait on hold
            if (user.isWaiting()) {
                customersHolding.addLast(user);

                System.out.printf("%s %s %s%n", EVENT_PUT_ON_HOLD, user.getName(), time);
            } else {
                System.out.printf("%s %s %s%n", EVENT_TRY_LATER, user.getName(), time);
            }
        }
    }

    /**
     * Creates a new Session with the given parameters, and updates their relevant
     * fields
     * 
     * @param rep
     *            the rep that will be assigned to the session
     * @param user
     *            the user that will be assigned to the session
     * @param time
     *            the time the session will be created at
     * @return the newly created session with the user and rep
     */
    private static Session assignRep (final Rep rep, final User user, final String time) {
        final Session session = new Session(user, rep);

        user.setChatSession(session);
        rep.setChatSession(session);

        // Mark rep & user as no longer waiting
        rep.setWaitingForCustomer(false);
        user.setWaiting(false);

        System.out.printf("%s %s %s %s%n", EVENT_REP_ASSIGN, user.getName(), rep.getName(), time);
        return session;
    }

    /**
     * Initialize the list of available reps using the list of reps given
     * 
     * @param reps
     *            names of all reps that will be available
     */
    static void initializeReps (final String[] reps) {
        for (final String r : reps) {
            availableReps.addLast(new Rep(r));
        }
    }

    /**
     * Parses given tokens and generates a user from them
     * 
     * @param tokens
     *            tokens[1] is time, tokens[2] is name, tokens[3] is if user will
     *            wait
     * @return newly created User from parsed tokens
     */
    static User createUser (final String[] tokens) {
        final int[] time = parseTime(tokens[1]);

        return new User(tokens[2], time[0], time[1], tokens[3].equals(INST_WAIT));
    }

    /**
     * Parses a given 4 digit time string and splits it into hours:minutes
     * 
     * @param time
     *            4 digit time to parse
     * @return the parsed time in hours and in minutes in the format [hours,
     *         minutes]
     */
    static int[] parseTime (final String time) {
        final int[] t = { Integer.parseInt(time.substring(0, 2)),
                Integer.parseInt(time.substring(2)) };
        return t;
    }

    // All possible, valid, instructions that can be read and executed
    static final String INST_CHAT_REQ = "ChatRequest";
    static final String INST_QUIT_ON_HOLD = "QuitOnHold";
    static final String INST_CHAT_END = "ChatEnded";
    static final String INST_REPS_AVAIL = "PrintAvailableRepList";
    static final String INST_MAX_WAIT = "PrintMaxWaitTime";

    // Given when a chat request is made
    static final String INST_WAIT = "wait";
    static final String INST_LATER = "later";

    // What will be output in response to instructions
    static final String EVENT_CHAT_REQ = "ChatRequest";
    static final String EVENT_REP_ASSIGN = "RepAssignment";
    static final String EVENT_PUT_ON_HOLD = "PutOnHold";
    static final String EVENT_TRY_LATER = "TryLater";
    static final String EVENT_QUIT_ON_HOLD = "QuitOnHold";
    static final String EVENT_CHAT_END = "ChatEnded";
    static final String EVENT_REPS_AVAIL = "AvailableRepList";
    static final String EVENT_MAX_WAIT = "MaxWaitTime";
}
