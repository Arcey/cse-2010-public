// return the index of item if found in the sorted array (ascending order) with size items , or -1 otherwise
int search1(int[] array, int item)
{
   for (int i = 0; i < array.length; i++) {
      if (array[i] == item) {
         return i;
      }
   }
   return -1;
}


import java.util.Arrays; 
int search2(int[] array, int item)
{

   int mid = Math.floor(array.length / 2);

   if (mid == 0) {
      return -1;
   }

   if (array[mid] == item) {
      return mid;
   }
   if (array[mid] > item) {
      search2(Arrays.copyOfRange(array, 0, mid), item);
   } else if (array[mid] < item) {
      search2(Arrays.copyOfRange(array, mid + 1, array.length), item);
   } 
}

class Person {
   int idNum;
   String name;
   String email;

   void setPerson(Person person, int idNum, String name, String email) {
      person.idNum = idNum;
      person.name = name;
      person.email = email;
   }
}

class Node {
   int data;
   Node next = null;
   Node head = null;

   void append(int item) {
      Node n = this;
      while (n.next != null) {
         n = n.next;
      }
      n.next = Node(item);
   }

   boolean remove(int item) {
      Node n = this;

      while (n != null) {
         
      }
   }
}
