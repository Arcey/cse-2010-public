# 2010 HW4

## What can be done better
[ ] Many of the methods in HW4 could (and should) be moved into being member methods of the Customer class

Hi,

I don't know if you remember but last week on Thursday you helped me log into the code01 server for the first time all semester at the end of lab to test a compilation error that was showing up on HW3. I decided to run my HW4 as well to see if there were any issues, and sure enough, I'm running into another. 

I chose to use a record to construct my Customer object for the assignment, since it was just more convenient. At the time I couldn't test the code on the code01, nor did I think it would be necessary. Running it on the server now, I'm finding out records don't seem to be supported on the server's version of Java either. I've attached the same Customers file, the only difference being it has been converted from a record to a normal class so that it will compile properly on the server. If you're willing to look past the versioning error I'd appreciate it, if this file counts as a late submission and tanks the assignment score then I'd rather it not be considered in the final submission and just take the non-compilation point loss.

Thanks for your help
