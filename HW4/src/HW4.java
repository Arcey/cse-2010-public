/*
    Author: Vincent Quintero
    Email: 
    Course: CSE 2010
    Section: 02
    Description of this file: Logic for recommending music to customers
*/

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

public class HW4 {
    public static void main(final String[] args) throws FileNotFoundException {
        final CustomersArray customers = new CustomersArray();
        final Customer target = readCustomers(new File(args[0]), customers);
        final HeapPriorityQueue<Double, String> customerDistances = calculateCustomerDistances(target, customers);
        final Scanner scanner = new Scanner(new File(args[1]));

        // Read in all words from the given file
        while (scanner.hasNextLine()) {
            final String[] line = scanner.nextLine().split(REGEX_MATCH_ALL_WHITESPACE);

            switch (line[0]) {
                case ACTION_ADDCUSTOMER:
                    final Customer c = parseCustomer(line, 1);
                    customers.insert(c);
                    customerDistances.insert(calcSongDistance(target.ratings(), c.ratings()), c.name());
                    System.out.printf("%s %s %s%n", ACTION_ADDCUSTOMER, c.name(), c.outputRatings());
                    break;
                case ACTION_PRINTCUSTOMERDISTANCERATINGS:
                    System.out.println(ACTION_PRINTCUSTOMERDISTANCERATINGS);
                    printCustomerDistances(target, customers);
                    break;
                case ACTION_RECOMMENDSONGS:
                    handleRecommendSongs(target, customers, customerDistances);
                    break;
            }
        }

        scanner.close();
    }

    /**
     * Determines what customer should & can recommend songs to the target customer
     * 
     * @param target            customer to be recommended to
     * @param customers         pool of potential recommenders
     * @param customerDistances list of "difference" in taste between target and
     *                          each customer
     */
    private static void handleRecommendSongs(final Customer target, final CustomersArray customers,
            final HeapPriorityQueue<Double, String> customerDistances) {

        // Closest customers should be at the top of the heap & already alphabetically
        // sorted
        final ArrayList<Entry<Double, String>> closestCustomers = new ArrayList<>();
        final ArrayList<Integer> unratedSongs = getUnratedIndices(target);
        boolean validClosest = false;
        Customer closestCustomer = null;

        while (!customerDistances.isEmpty() && !validClosest) {
            closestCustomers.add(customerDistances.removeMin());
            closestCustomer = customers.find(closestCustomers.get(closestCustomers.size() - 1).getValue());

            validClosest = validClosestCustomer(unratedSongs, closestCustomer.ratings());
        }

        if (validClosest && closestCustomer != null) {
            System.out.printf("%s %s%s%n", ACTION_RECOMMENDSONGS, closestCustomer.name(),
                    recommendedSongs(unratedSongs, closestCustomer.ratings()));
        } else {
            System.out.printf("%s %s%n", ACTION_RECOMMENDSONGS, NO_SONGS_RECOMMENDED);
        }

        // Repopulate all removed customers back into the priority queue
        for (final Entry<Double, String> e : closestCustomers) {
            customerDistances.insert(e.getKey(), e.getValue());
        }
    }

    /**
     * Goes through every song rated and checks if it meets some criteria. If so, it
     * is recommended
     * 
     * @param unratedSongs
     *                     indices of songs that have not been rated
     * @param ratings
     *                     to check at each index to see if a song was rated
     *                     satisfactorily
     * @return the songs recommended and their recommended rating
     */
    private static String recommendedSongs(final ArrayList<Integer> unratedSongs, final int[] ratings) {
        final StringBuilder sb = new StringBuilder();

        for (final int song : unratedSongs) {
            if (ratings[song] >= REC_SONG_THRESHOLD) {
                sb.append(" ").append(SONG_NAME).append(song).append(" ").append(ratings[song]);
            }
        }

        return sb.toString();
    }

    /**
     * Looks for the first song in ratings using the unratedSong indices that meets
     * some criteria
     * 
     * @param unratedSongs
     *                     indices of songs that have not been rated
     * @param ratings
     *                     to check at each index to see if a song was rated
     *                     satisfactorily
     * @return true if any song in ratings meets the criteria, false otherwise
     */
    private static boolean validClosestCustomer(final ArrayList<Integer> unratedSongs, final int[] ratings) {
        for (final int i : unratedSongs) {
            // As long as at least one song meets the threshold, it is a valid customer
            // recommendation
            if (ratings[i] >= REC_SONG_THRESHOLD) {
                return true;
            }
        }
        return false;
    }

    /**
     * Go through the target's ratings and collect the indices of any unrated songs
     * 
     * @param target
     *               the customer who's ratings are being evaluated
     * @return an arraylist of indices of unrated songs (if any)
     */
    private static ArrayList<Integer> getUnratedIndices(final Customer target) {
        final ArrayList<Integer> indices = new ArrayList<Integer>();
        final int[] ratings = target.ratings();

        for (int i = 0; i < ratings.length; i++) {
            if (ratings[i] == UNRATED_SONG) {
                indices.add(i);
            }
        }

        return indices;
    }

    /**
     * Calculates each customer in the array's distance from the target customer's
     * music tastes
     * 
     * @param target
     *                  customer to compare against
     * @param customers
     *                  customers to compare against target
     * @return HeapPriorityQueue<Double, String> with the customers with the closest
     *         distance at the front (top)
     */
    private static HeapPriorityQueue<Double, String> calculateCustomerDistances(final Customer target,
            final CustomersArray customers) {
        final HeapPriorityQueue<Double, String> customerDistances = new HeapPriorityQueue<>();

        for (int i = 0; i < customers.size(); i++) {
            final Customer c = customers.get(i);
            customerDistances.insert(calcSongDistance(target.ratings(), c.ratings()), c.name());
        }

        return customerDistances;
    }

    /**
     * Print customer distances, ratings and names relative to the target customer
     * 
     * @param target
     *                  specially printed customer every distance is from
     * @param customers
     *                  customers with distances to print
     */
    private static void printCustomerDistances(final Customer target, final CustomersArray customers) {
        System.out.printf("%-6.3s%-10s%20s%n", "", target.name(), target.outputRatings());

        for (int i = 0; i < customers.size(); i++) {
            final double rating = calcSongDistance(target.ratings(), customers.get(i).ratings());

            if (rating == FAIL_DISTANCE_CALCULATION) {
                System.out.printf("%-6s%-10s%20s%n", NO_DISTANCE, customers.get(i).name(),
                        customers.get(i).outputRatings());
            } else {
                System.out.printf("%-6.3f%-10s%20s%n", rating, customers.get(i).name(),
                        customers.get(i).outputRatings());
            }
        }
    }

    /**
     * Reads customers from a file and converts them to objects
     * 
     * @param f
     *               file to read customers from
     * @param target
     *               a placeholder for the parsed target customer
     * @return the list of created customers
     * @throws FileNotFoundException
     */
    private static Customer readCustomers(final File f, final CustomersArray customers)
            throws FileNotFoundException {

        final Scanner scanner = new Scanner(f);

        // First line is ALWAYS the target customer
        final Customer target = parseCustomer(scanner.nextLine().split(REGEX_MATCH_ALL_WHITESPACE));

        // Populate remaining customers
        while (scanner.hasNextLine()) {
            customers.insert(parseCustomer(scanner.nextLine().split(REGEX_MATCH_ALL_WHITESPACE)));
        }

        scanner.close();
        return target;
    }

    /**
     * Create a customer from the tokens in a given line (Assumed in order NAME
     * rating1 rating2...)
     * 
     * @param line
     *              tokens to create customer from
     * @param start
     *              index where the customer's information begins
     * @return
     */
    private static Customer parseCustomer(final String[] line, final int start) {
        return new Customer(line[start],
                Arrays.stream(line, start + 1, line.length)
                        .mapToInt(Integer::parseInt).toArray());
    }

    // Convenience method to create a customer with a start of index 0
    private static Customer parseCustomer(final String[] line) {
        return parseCustomer(line, 0);
    }

    /**
     * Calculate the distance between song selections using 1/s + 1/s * SUM(ABS(x[i]
     * - y[i]))
     * 
     * @param x
     *          array of song ratings for customer1
     * @param y
     *          array of song ratings for customer2
     * @return the distance between x & y's music tastes
     */
    private static double calcSongDistance(final int[] x, final int[] y) {
        final int[] shared = commonSongs(x, y);
        final double s = shared.length;

        // There are no ratings shared between the two lists
        if (s == 0)
            return FAIL_DISTANCE_CALCULATION;

        double sum = 0;
        for (final int i : shared) {
            sum += Math.abs(x[i] - y[i]);
        }

        return (1 / s) + (sum / s);
    }

    /**
     * Finds and aggregates the indices of songs that are nonzero in BOTH lists
     * 
     * @param x
     *          array of song ratings for customer1
     * @param y
     *          array of song ratings for customer2
     * @return array of indices that are nonzero in both lists
     */
    private static int[] commonSongs(final int[] x, final int[] y) {
        return IntStream.range(0, MAX_SONGS).filter(i -> x[i] > 0 && y[i] > 0).toArray();
    }

    public static final int MAX_SONGS = 10;
    public static final int UNRATED_SONG = 0;
    public static final int REC_SONG_THRESHOLD = 4; // Minimum number a song can be rated and still be recommended
    public static final int FAIL_DISTANCE_CALCULATION = -1;
    public static final String ACTION_ADDCUSTOMER = "AddCustomer";
    public static final String ACTION_RECOMMENDSONGS = "RecommendSongs";
    public static final String ACTION_PRINTCUSTOMERDISTANCERATINGS = "PrintCustomerDistanceRatings";
    public static final String SONG_NAME = "song";
    public static final String NO_DISTANCE = "-----";
    public static final String NO_SONGS_RECOMMENDED = "none";
    public static final String REGEX_MATCH_ALL_WHITESPACE = "\\s+";
}
