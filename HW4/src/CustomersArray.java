import java.util.Arrays;

public class CustomersArray {
    public static final int MAX_CUSTOMERS = 100;

    private final Customer[] customers = new Customer[MAX_CUSTOMERS];
    private int size = 0;

    public CustomersArray() {
    }

    /**
     * Get the number of non-null elements in the array
     * 
     * @return number of customers in the array
     */
    public int size() {
        return size;
    }

    /**
     * Insert a customer into the array according to its natural ordering
     * 
     * @param customer to be inserted into the array
     * @return true
     */
    public boolean insert(final Customer customer) {
        customers[size] = customer;
        size++;
        Arrays.sort(this.customers, 0, size);
        return true;
    }

    /**
     * Find a customer by name via a binary search
     * 
     * @param name of customer to find
     * @return the customer if it exists, null otherwise
     */
    public Customer find(final String name) {
        final int index = binarySearch(name, 0, this.size);
        if (index == -1) {
            return null;
        }
        return this.customers[index];
    }

    /**
     * Returns the customer at a specified index
     * 
     * @param index of customer to return
     * @return customer at index or null if not found
     */
    public Customer get(final int index) {
        return this.customers[index];
    }

    /**
     * Perform a binary search on the array to locate a customer by name
     * 
     * @param name  of customer to be found
     * @param first position to begin searching
     * @param last  position to stop searching
     * @return the index of the customer if it exists, else -1
     */
    private int binarySearch(final String name, final int first, final int last) {
        if (first > last) { // Start out of array bounds
            return -1;
        }

        final int mid = first + (last - first) / 2;
        final int comparison = this.customers[mid].name().compareTo(name);

        if (comparison == 0) {
            return mid;
        }

        if (comparison > 0) {
            return binarySearch(name, first, mid - 1);
        } else {
            return binarySearch(name, mid + 1, last);
        }
    }
}
