public class Customer implements Comparable<Customer> {
    final String name;
    final int[] ratings;

    public Customer(final String name, final int[] ratings) {
        this.name = name;
        this.ratings = ratings;
    }

    // Returns this customer's ratings in a print-ready string format
    public String outputRatings() {
        final StringBuilder sb = new StringBuilder();

        for (int i = 0; i < this.ratings.length; i++) {
            sb.append(this.ratings[i]);
            if (i != this.ratings.length - 1) {
                sb.append(' ');
            }
        }

        return sb.toString();
    }

    @Override
    public int compareTo(Customer o) {
        return this.name.compareTo(o.name);
    }

    public String name() {
        return name;
    }

    public int[] ratings() {
        return ratings;
    }
}
