/*
Author: Vincent Quintero
Email: 
Course: CSE 2010
Section: 02
Description of this file: Game object managing all aspects of the game and its logic
*/

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.TreeSet;
import java.util.function.BiPredicate;

public class Game {
    // Constants defining what characters constitute a player, obstacle, etc.
    public static final String PLAYER = "[0-9]"; // Regex matching all positive integers
    public static final String CREATURE = "[A-Z]"; // Regex matching all letters A through Z
    public static final char OBSTACLE = '#';
    public static final char DESTINATION = '*';
    public static final char EMPTY_SPACE = ' ';

    private Graph<GameTile> gamestate; // The true "brains" behind the game
    private GameTile[][] map; // The visual layer that is used to let player interact with gamestate
    private Entity player;
    private Entity objective;
    private GameState status;
    private final int gameRows;
    private final int gameCols;
    private final boolean hard; // Determines the pathfinding algorithm creatures will hunt the player with
    private final TreeSet<Entity> creatures; // Store existing creatures alphabetically for easy turn order

    /**
     * Barebones initialization of a normal game
     * 
     * @param rows in the game board
     * @param cols in the game board
     */
    public Game(int rows, int cols) {
        this(rows, cols, false);
    }

    /**
     * Canonical constructor prepping all necessary game fields
     * 
     * @param rows in the game board
     * @param cols in the game board
     * @param hard false=simple creatures, true=advanced creatures
     */
    public Game(int rows, int cols, boolean hard) {
        this.hard = hard;
        this.gameRows = rows;
        this.gameCols = cols;
        this.status = GameState.PLAYING;
        this.player = null;
        this.gamestate = new Graph<>();
        this.creatures = new TreeSet<>();
        this.map = new GameTile[this.gameRows][this.gameCols];
    }

    /**
     * @return the graph that contains all actionable game information
     */
    private Graph<GameTile> getGameState() {
        return this.gamestate;
    }

    /**
     * Inserts the given gametile into the visual map and gamestate.
     * Obstacles will only be inserted into the map and not into the
     * gamestate as they can ruin the pathfinding algorithm used
     * 
     * @param tile to insert into the game
     * @param r    row the tile is at
     * @param c    column the tile is at
     * @return true if the tile was successfully inserted, false otherwise
     */
    private boolean insertTile(GameTile tile, int r, int c) {
        if (withinBounds(r, c)) { // Only add if its a valid tile
            this.map[r][c] = tile;
            if (!tile.isObstacle()) {
                this.gamestate.insert(tile);
            }
            return true;
        }
        return false;
    }

    /**
     * Get the GameTile at the specified position
     * 
     * @param r row the tile is at
     * @param c column the tile is at
     * @return GameTile at the specified position or null if non-existent
     */
    private GameTile getTile(int r, int c) {
        if (withinBounds(r, c)) {
            return this.map[r][c];
        }
        return null;
    }

    /**
     * Check if the given position is a valid location in the world
     * 
     * @param r row the tile is at
     * @param c column the tile is at
     * @return true if the position is valid, false otherwise
     */
    private boolean withinBounds(int r, int c) {
        if (r >= this.gameRows || c >= this.gameCols || r < 0 || c < 0) {
            return false;
        }
        return true;
    }

    /**
     * Store the given creature in the game's memory
     * 
     * @param tile position of the creature to store
     * @return true on successful store
     */
    private boolean addCreature(GameTile tile) {
        return this.creatures.add(new Entity(tile.getData(), tile));
    }

    /**
     * Store the given player in the game's memory
     * 
     * @param tile position of the player to store
     * @return true on successful store
     */
    private boolean addPlayer(GameTile tile) {
        this.player = new Entity(tile.getData(), tile);
        return true;
    }

    /**
     * Store the given objective in the game's memory
     * 
     * @param tile position of the objective to store
     * @return true on successful store
     */
    private boolean addObjective(GameTile tile) {
        this.objective = new Entity(tile.getData(), tile);
        return true;
    }

    /**
     * Check if the given coordinates are an obstacle in the visual map
     * 
     * @param r row the tile is at
     * @param c column the tile is at
     * @return true if the coordinates are an obstacle, false otherwise
     */
    private boolean isObstacle(int r, int c) {
        return getTile(r, c).isObstacle();
    }

    /**
     * Check if the given position is a valid space for the player to move.
     * This method does not perform bounds checking as it is expected
     * to have been done before calling this method.
     * 
     * @param r row the tile is at
     * @param c column the tile is at
     * @return true if the position given is objective or empty, false otherwise
     */
    private boolean isValidPlayerSpace(int r, int c) {
        return getTile(r, c).isObjective() || getTile(r, c).isEmpty() || getTile(r, c).isCreature();
    }

    /**
     * Check if the given position is a valid space for the player to move to
     * 
     * @param r row the tile is at
     * @param c column the tile is at
     * @return true if the position is a valid place to move, false otherwise
     */
    private boolean validPlayerMove(int r, int c) {
        return withinBounds(r, c) && isValidPlayerSpace(r, c);
    }

    /**
     * Check if the given position is a valid space for a creature to move.
     * This method does not perform bounds checking as it is expected
     * to have been done before calling this method.
     * 
     * @param r row the tile is at
     * @param c column the tile is at
     * @return true if the position given is a player or empty, false otherwise
     */
    private boolean isValidCreatureMove(int r, int c) {
        return getTile(r, c).isEmpty() || getTile(r, c).isPlayer();
    }

    /**
     * Check if the given position is a valid space for a creature to move to
     * 
     * @param r row the tile is at
     * @param c column the tile is at
     * @return true if the position is a valid place to move, false otherwise
     */
    private boolean validCreatureMove(int r, int c) {
        return withinBounds(r, c) && isValidCreatureMove(r, c);
    }

    /**
     * Removes all creatures from the gamestate except for the given one.
     * This ensures a creature will not try to pathfind onto another
     * creature during searches.
     * This method assumes the given creature is valid and exists
     * 
     * @param creature that will not be hidden from the gamestate
     */
    private void hideCreaturesExcept(Entity creature) {
        // Remove all other creature's tiles from gamestate since they're occupied
        for (Entity e : this.creatures) {
            if (e != creature) {
                this.getGameState().deleteVertex(e.getTile());
            }
        }
    }

    /**
     * Restores all creatures to the gamestate except for the given one.
     * This recreates and reconnects all creatures except for the given one
     * that were previously hidden. Unexpected results will occur if called
     * before hideCreaturesExcept().
     * This method assumes the given creature is valid and exists
     * 
     * @param creature that will not be restored to the gamestate
     */
    private void unhideCreaturesExcept(Entity creature) {
        // Place all removed creatures back in the gamestate
        for (Entity e : this.creatures) {
            if (e != creature) {
                this.insertTile(e.getTile(), e.getTile().getY(), e.getTile().getX());
            }
        }

        // Reconnect any lost edges
        this.creatures.forEach(e -> connectTile(e.getTile().getY(), e.getTile().getX()));
    }

    /**
     * Swaps the data of the given tiles, does nothing to their positions.
     * New copies of the data must be made otherwise issues arise when a
     * player attempts to backtrack in their path.
     * 
     * @param a tile to be swapped
     * @param b tile to be swapped
     */
    private void swapTileData(GameTile a, GameTile b) {
        final char tmp = Character.valueOf(a.getData());
        a.setData(Character.valueOf(b.getData()));
        b.setData(tmp);
    }

    /**
     * Take a given list of tiles and convert into a string representing
     * each tile's position in the format described in the instructions.
     * 
     * @param path to stringify
     * @return stringified coordinates of each tile in the given list
     */
    private String stringifyPath(List<GameTile> path) {
        final StringBuilder sb = new StringBuilder();

        for (int i = 0; i < path.size(); i++) {
            final GameTile tile = path.get(i);
            sb.append("(").append(tile.getY()).append(",").append(tile.getX()).append(")");

            if (i != path.size() - 1) {
                sb.append(" ");
            }
        }

        return sb.toString();
    }

    /**
     * Connects a GameTile stored at the given position to all adjacent GameTiles
     * that are not obstacles and within the world bounds. Order of execution is
     * important to meet the instruction's desired U/D/L/R ordering requirements.
     * 
     * @param r row the tile is at
     * @param c column the tile is at
     */
    private void connectTile(int r, int c) {
        // Ensure this point is not out of bounds and is not an obstacle
        if (!withinBounds(r, c) || isObstacle(r, c)) {
            return;
        }

        // * Ensure all edges are new for this tile (stuff breaks w/o this I'm scared)
        this.gamestate.deleteAllEdges(getTile(r, c));

        // Adjacent top exists and is not an obstacle
        if (withinBounds(r - 1, c) && !isObstacle(r - 1, c)) {
            this.gamestate.insertEdge(getTile(r, c), getTile(r - 1, c), 1);
        }
        // Adjacent bottom exists and is not an obstacle
        if (withinBounds(r + 1, c) && !isObstacle(r + 1, c)) {
            this.gamestate.insertEdge(getTile(r, c), getTile(r + 1, c), 2);
        }
        // Adjacent left exists and is not an obstacle
        if (withinBounds(r, c - 1) && !isObstacle(r, c - 1)) {
            this.gamestate.insertEdge(getTile(r, c), getTile(r, c - 1), 3);
        }
        // Adjacent right exists and is not an obstacle
        if (withinBounds(r, c + 1) && !isObstacle(r, c + 1)) {
            this.gamestate.insertEdge(getTile(r, c), getTile(r, c + 1), 4);
        }
    }

    /**
     * Convenience method to restore the gamestate back to its original state
     */
    private void resetWeights() {
        // Reset edge weights for next traversal
        for (int i = 0; i < this.getGameRows(); i++) {
            for (int j = 0; j < this.getGameCols(); j++) {
                this.connectTile(i, j);
            }
        }
    }

    /**
     * Assigns weights to each tile in the gamestate based on how closely the
     * tile aligns with the given playerPath.
     * 
     * @param playerPath to follow when weighing interception points
     */
    private void mapInterceptionPoints(List<GameTile> playerPath) {
        // For every vertex in an edge that's in the path, reduce its weight by 1
        for (int i = 0; i < this.gameRows; i++) {
            for (int j = 0; j < this.gameCols; j++) {
                final List<List<GameTile>> edges = this.getGameState().edges(getTile(i, j));

                for (List<GameTile> pair : edges) {
                    final GameTile source = pair.get(0);
                    final GameTile destination = pair.get(1);
                    int weight = 3;

                    if (playerPath.contains(source)) {
                        weight--;
                    }

                    if (playerPath.contains(destination)) {
                        weight--;
                    }

                    // Update the weight of this edge with the newly calculated weight
                    this.getGameState().getEdge(source, destination).setWeight(
                            this.getGameState().getEdge(source, destination).getWeight() + weight);
                    // weight);
                }
            }
        }
    }

    /**
     * Test moving the given entity in the given direction to determine if the move
     * would be valid based on the given condition.
     * 
     * @param entity    to be moved
     * @param dir       to move the entity in
     * @param validator to test if the move is valid
     * @return the tile that the entity would be at if the move was successful, or
     *         null if the move is invalid
     */
    private GameTile tryMove(GameTile entity, Move dir, BiPredicate<Integer, Integer> validator) {
        int nextRow = entity.getY();
        int nextCol = entity.getX();

        switch (dir) {
            case DOWN:
                nextRow += 1;
                break;
            case LEFT:
                nextCol -= 1;
                break;
            case RIGHT:
                nextCol += 1;
                break;
            case UP:
                nextRow -= 1;
                break;
            default:
                return null; // Invalid move direction
        }

        if (validator.test(nextRow, nextCol)) {
            final GameTile newTile = getTile(nextRow, nextCol);
            return newTile;
        }

        // Invalid move
        return null;
    }

    /**
     * Determine the direction the current must move to reach the target
     * 
     * @param current position
     * @param target  position
     * @return a Move object representing the direction that target is in relative
     *         to current
     */
    private Move getMoveDirection(GameTile current, GameTile target) {
        // Y = col, X = row
        if (current.getY() < target.getY()) {
            return Move.DOWN;
        } else if (current.getY() > target.getY()) {
            return Move.UP;
        } else if (current.getX() < target.getX()) {
            return Move.RIGHT;
        } else if (current.getX() > target.getX()) {
            return Move.LEFT;
        }
        return null;
    }

    /**
     * Helper method to determine the shortest path to the player from a given tile
     * 
     * @param from tile that you are starting from
     * @return the shortest path to the player from the given starting position
     */
    private List<GameTile> shortestPathToPlayer(GameTile from) {
        // return this.getGameState().dijkstra(from, this.player.getTile());
        return this.getGameState().BFS(from, this.player.getTile());
    }

    /**
     * Determine the path a given simple creature should take to reach the player
     * 
     * @param creature to find a path for
     * @return the simple path from the creature to the player
     */
    private List<GameTile> generateSimpleCreaturePath(Entity creature) {
        // Remove all other creatures from the gamestate so their tiles aren't
        // overlapped
        hideCreaturesExcept(creature);

        // Go through every tile in the map and reconnect it if working with advanced
        // creatures
        final List<GameTile> path = shortestPathToPlayer(creature.getTile());

        // printPreviousLinks();
        unhideCreaturesExcept(creature);
        resetWeights();

        return path;
    }

    /**
     * Move each creature one after another along their generated paths towards the
     * player
     * 
     * @return true when all creatures have moved
     */
    private boolean moveSimpleCreatures() {
        for (Entity creature : this.creatures) {
            final List<GameTile> path = generateSimpleCreaturePath(creature);

            if (path.size() > 1) {
                final Move m = getMoveDirection(creature.getTile(), path.get(1));
                final GameTile tile = tryMove(creature.getTile(), m, this::validCreatureMove);
                this.status = tile.isPlayer() ? GameState.LOST : GameState.PLAYING;

                swapTileData(creature.getTile(), tile);
                creature.setTile(tile);
                System.out.printf("%s %s: %s %d %s%n", CREATURE_STRING, creature.getId(), moveToString(m),
                        path.size() - 1, stringifyPath(path));
            }
        }

        return true;
    }

    /**
     * Determine the path for each creature on the board before moving any of them.
     * 
     * @return a list of paths to the player for each creature on the board
     */
    private List<List<GameTile>> generateAdvancedCreaturePaths() {
        final List<GameTile> playerPath = this.getGameState().BFS(this.player.getTile(), this.objective.getTile());
        final List<List<GameTile>> creaturePaths = new LinkedList<>();

        mapInterceptionPoints(playerPath);

        // This may be unnecessarily inefficient but it works 
        for (Entity creature : this.creatures) {

            // Hide objective tile from creatures, they can't use it
            this.getGameState().deleteVertex(this.objective.getTile());

            // Remove all other creatures from the gamestate so their tiles aren't overlapped
            hideCreaturesExcept(creature);

            creaturePaths.add(this.getGameState().dijkstra(creature.getTile(), this.player.getTile()));

            unhideCreaturesExcept(creature);

            this.insertTile(this.objective.getTile(), this.objective.getTile().getY(), this.objective.getTile().getX());
            resetWeights();
        }

        return creaturePaths;
    }

    /**
     * Generate a move for every creature on the board, only performing the moves
     * after a move has been generated for each of them.
     * 
     * @return true when all creatures have moved
     */
    private boolean moveAdvancedCreatures() {
        final List<List<GameTile>> creaturePaths = generateAdvancedCreaturePaths();

        int i = 0;
        for (Entity creature : this.creatures) {
            final List<GameTile> path = creaturePaths.get(i);

            if (path.size() > 1) {
                // 1st index of path is the next move as 0th index is current position
                final Move m = getMoveDirection(creature.getTile(), path.get(1));
                final GameTile tile = tryMove(creature.getTile(), m, this::validCreatureMove);
                this.status = tile.isPlayer() ? GameState.LOST : GameState.PLAYING;

                swapTileData(creature.getTile(), tile);
                creature.setTile(tile);
                System.out.printf("%s %s: %s %d %s%n", CREATURE_STRING, creature.getId(), moveToString(m),
                        path.size() - 1,
                        stringifyPath(path));
            }
            i++;
        }

        return true;
    }

    /**
     * Initialize a Game from the given file
     * 
     * @param f    file to generate the graph from
     * @param hard difficulty to run the game at
     * @return the game generated from the given file
     * @throws FileNotFoundException bad file passed in
     */
    private static Game loadFromFile(File f, boolean hard) throws FileNotFoundException {
        final Scanner scanner = new Scanner(f);
        final int rows = scanner.nextInt();
        final int cols = scanner.nextInt();
        final Game g = new Game(rows, cols, hard);

        // Clear hanging newline after reading ints
        scanner.nextLine();

        // Create graph and visual map from the gamestate file
        for (int i = 0; i < rows; i++) {
            final char[] line = scanner.nextLine().toCharArray();

            for (int j = 0; j < line.length; j++) {
                final GameTile gt = new GameTile(line[j], j, i);
                g.insertTile(gt, i, j);

                if (gt.isCreature()) {
                    g.addCreature(gt);
                } else if (gt.isPlayer()) {
                    g.addPlayer(gt);
                } else if (gt.isObjective()) {
                    g.addObjective(gt);
                }
            }
        }
        scanner.close();
        return g;
    }

    /**
     * Load game from a preexisting game file
     * 
     * @param f    file to generate the graph from
     * @param hard difficulty to run the game at
     * @return the game generated from the given file
     * @throws FileNotFoundException bad file passed in
     */
    public static Game loadGamestate(File f, boolean hard) throws FileNotFoundException {
        final Game g = loadFromFile(f, hard);

        // Create edges between all adjacent non-obstacle game tiles in the graph
        for (int i = 0; i < g.getGameRows(); i++) {
            for (int j = 0; j < g.getGameCols(); j++) {
                g.connectTile(i, j);
            }
        }

        return g;
    }

    /**
     * Convert a given Move to its string representation
     * 
     * @param m to stringify
     * @return string representation of the move
     */
    public static String moveToString(Move m) {
        switch (m) {
            case UP:
                return MOVE_UP;
            case DOWN:
                return MOVE_DOWN;
            case LEFT:
                return MOVE_LEFT;
            case RIGHT:
                return MOVE_RIGHT;
            default:
                return null;
        }
    }

    /**
     * Convert a given string to its corresponding Move
     * 
     * @param m to Move-ify
     * @return Move datatype from the given move
     */
    public static Move stringToMove(String m) {
        switch (m) {
            case MOVE_UP:
                return Move.UP;
            case MOVE_DOWN:
                return Move.DOWN;
            case MOVE_LEFT:
                return Move.LEFT;
            case MOVE_RIGHT:
                return Move.RIGHT;
            default:
                return Move.INVALID;
        }
    }

    /**
     * Tries and performs a given move for player
     * 
     * @param move direction to move player in
     * @return true if the move was successful, false if unsuccessful
     */
    public boolean movePlayer(Move move) {
        final GameTile tile = tryMove(this.player.getTile(), move, this::validPlayerMove);

        // A valid direction was given
        if (tile != null) {
            if (tile.isObjective()) { // Moved onto objective, game is won
                tile.setData(EMPTY_SPACE); // So there will be nothing shown when swapped
                this.status = GameState.WON;
            } else if (tile.isCreature()) {
                this.status = GameState.LOST;
            }

            swapTileData(this.player.getTile(), tile);
            this.player.setTile(tile);
            return true;
        }

        return false;
    }

    /**
     * Performs moves for all available creatures based on the game's difficulty
     * 
     * @return true upon the completion of the creature's moves
     */
    public boolean moveCreatures() {
        return this.hard ? moveAdvancedCreatures() : moveSimpleCreatures();
    }

    /**
     * @return the player in the game
     */
    public Entity getPlayer() {
        return this.player;
    }

    /**
     * 
     * @return the number of rows in the game board
     */
    public int getGameRows() {
        return gameRows;
    }

    /**
     * 
     * @return the number of columns in the game board
     */
    public int getGameCols() {
        return gameCols;
    }

    /**
     * @return the game's current state
     */
    public GameState status() {
        return this.status;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(" ");

        for (int i = 0; i < this.gameCols; i++) {
            sb.append(i);
        }

        sb.append('\n');

        for (int i = 0; i < this.gameRows; i++) {
            sb.append(i);
            for (int j = 0; j < this.gameCols; j++) {
                sb.append(this.map[i][j]);
            }
            // Prevent trailing newline
            sb.append(i != this.gameRows - 1 ? "\n" : "");
        }

        return sb.toString();
    }

    private static final String CREATURE_STRING = "Creature";
    private static final String MOVE_UP = "u";
    private static final String MOVE_DOWN = "d";
    private static final String MOVE_LEFT = "l";
    private static final String MOVE_RIGHT = "r";

    public static enum GameState {
        WON, LOST, PLAYING,
    }

    public enum Move {
        UP,
        DOWN,
        LEFT,
        RIGHT,
        INVALID,
    }    
}
