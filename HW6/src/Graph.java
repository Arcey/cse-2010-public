/*
Author: Vincent Quintero
Email: 
Course: CSE 2010
Section: 02
Description of this file: Implementation of a Graph object and its necessary classes
*/

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class Graph<T> {
    // Used for weighting in dijkstra's. MAX_VALUE causes overflow so this is a
    // happy medium while hopefully keeping everything accurate
    private static final int POS_INF = Integer.MAX_VALUE / 2;
    private final List<Vertex<T>> graph;

    /**
     * Constructor to create an empty graph
     */
    public Graph() {
        graph = new LinkedList<>();
    }

    /**
     * Return the vertex containing the given object if it exists
     * 
     * @param v object in a vertex to find
     * @return the vertex containing the given object or null if it doesn't exist
     */
    private Vertex<T> getVertex(T v) {
        for (Vertex<T> t : this.graph) {
            if (t.getElement() == v) {
                return t;
            }
        }

        return null;
    }

    /**
     * Create a one-way edge between the given objects if both are vertices
     * 
     * @param a object in the source vertex
     * @param b object in the destination vertex
     * @param w weight of the edge connecting source and destination
     * @return true upon edge creation
     */
    public boolean insertEdge(T a, T b, int w) {
        // Only add the edge if it doesn't already exist
        if (getEdge(a, b) == null) {
            getVertex(a).addEdge(b, w);
        }

        return true;
    }

    /**
     * Delete all outgoing edges from the vertex containing the given object
     * 
     * @param v object to remove outgoing edges from
     * @return true upon edge deletion, false if no vertex v exists
     */
    public boolean deleteAllEdges(T v) {
        final Vertex<T> vt = getVertex(v);
        if (vt != null) {
            return vt.deleteAllEdges();
        }
        return false;
    }

    /**
     * Delete the vertex with the given object from the graph.
     * This also ensures all incoming edges are appropriately deleted.
     * 
     * @param v object in vertex to be deleted
     */
    public void deleteVertex(T v) {
        final Vertex<T> vertex = getVertex(v);

        if (vertex == null) {
            return;
        }

        for (Edge<T> e : vertex.getEdges()) {
            // Delete all edges with this vertex as a destination
            final Vertex<T> u = getVertex(e.getDestination());
            u.deleteEdge(e);
        }

        // Delete the vertex itself
        this.graph.remove(vertex);
    }

    /**
     * Determine if the two vertices of the given objects are connected
     * 
     * @param u object in vertex u
     * @param v object in vertex v
     * @return true if the two vertices are connected, false otherwise
     */
    public boolean areAdjacent(T u, T v) {
        return getEdge(u, v) != null;
    }

    /**
     * Create a list of lists of edges containing all vertices connected to a given
     * vertex
     * 
     * @param v object in vertex who's edges will be returned
     * @return list of list of edges (if any)
     */
    public List<List<T>> edges(T v) {
        // 0 is source, 1 is destination, always returns a pair or null
        final List<List<T>> edges = new LinkedList<>();
        final Vertex<T> vt = getVertex(v);

        if (vt != null) {
            for (Edge<T> e : vt.getEdges()) {
                final LinkedList<T> pair = new LinkedList<>();
                pair.add(e.getSource());
                pair.add(e.getDestination());
                edges.add(pair);
            }
        }

        return edges;
    }

    /**
     * 
     * @return the number of vertices in this graph
     */
    public int numVertices() {
        return this.graph.size();
    }

    /**
     * 
     * @return the number of edges in this graph
     */
    public int numEdges() {
        int edges = 0;
        for (Vertex<T> v : this.graph) {
            edges += v.getEdges().size();
        }
        return edges;
    }

    /**
     * Get the edge connecting the given vertices if it exists.
     * 
     * @param u object in source vertex
     * @param v object in destination vertex
     * @return the edge connecting the given vertices if it exists or null if none
     */
    public Edge<T> getEdge(T u, T v) {
        final Vertex<T> vt = getVertex(u);

        if (vt != null) {
            final List<Edge<T>> edges = vt.getEdges();
            if (edges != null) {
                for (Edge<T> e : edges) {
                    if (e.getDestination() == v) {
                        return e;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Add an object into the graph
     * 
     * @param e object to add into the graph
     * @return true if the object was added, false otherwise
     */
    public boolean insert(T e) {
        return this.graph.add(new Vertex<T>(e));
    }

    /**
     * Implementation of dijkstra's algorithm to find the shortest path between
     * two vertices in the graph.
     * 
     * @param start object in vertex to start at
     * @param end   object in vertex to end at
     * @return the shortest path between the start and end vertices
     */
    public List<T> dijkstra(T start, T end) {
        final HashMap<T, Integer> weights = new HashMap<>();
        final HashMap<T, Boolean> visited = new HashMap<>();
        final HeapPriorityQueue<Integer, Vertex<T>> queue = new HeapPriorityQueue<>();

        this.graph.forEach(e -> {
            weights.put(e.getElement(), POS_INF);
            visited.put(e.getElement(), false);
        });

        // Initialize start vertex
        weights.put(start, 0);
        // visited.put(start, true);

        // Contain elements NOT in the cloud in the queue
        weights.forEach((v, d) -> queue.insert(d, getVertex(v)));

        while (!queue.isEmpty()) {
            final Vertex<T> u = queue.removeMin().getValue();

            if (u.getElement() == end) {
                break;
            }

            // Has not yet been visited
            if (!visited.get(u.getElement())) {
                for (Edge<T> edge : u.getEdges()) {
                    final Vertex<T> v = getVertex(edge.getDestination());
                    if (weights.get(u.getElement()) + edge.getWeight() < weights.get(v.getElement())) {
                        queue.delete(weights.get(v.getElement()), v);
                        weights.put(v.getElement(), weights.get(u.getElement()) + edge.getWeight());
                        queue.insert(weights.get(v.getElement()), v);
                        v.setPrev(u);
                    }
                }
                visited.put(u.getElement(), true);
            }
        }

        final List<T> path = shortestPath(start, end);

        // Delete all previous vertices to ensure no stragglers for future traversals
        this.graph.forEach(e -> e.setPrev(null));

        return path;
    }

    /**
     * Implementation of a breadth-first search algorithm to find the shortest path
     * between two vertices in the graph.
     * 
     * @param start object in vertex to start at
     * @param end   object in vertex to end at
     * @return the shortest path between the start and end vertices
     */
    public List<T> BFS(T start, T end) {
        final HashMap<T, Boolean> visited = new HashMap<>();
        final LinkedList<T> queue = new LinkedList<>();

        this.graph.forEach(e -> {
            visited.put(e.getElement(), false);
        });

        visited.put(start, true);
        queue.add(start);

        while (!queue.isEmpty()) {
            final Vertex<T> v = getVertex(queue.poll());

            if (v == getVertex(end)) {
                break;
            }

            for (Edge<T> e : v.getEdges()) {
                final T u = e.getDestination();

                if (!visited.get(u)) {
                    visited.put(u, true);
                    queue.add(u);
                    getVertex(u).setPrev(v);
                }
            }
        }
        final List<T> path = shortestPath(start, end);

        // Delete all previous vertices to ensure no stragglers for future traversals
        this.graph.forEach(e -> e.setPrev(null));

        return path;
    }

    /**
     * Traverse through the graph starting at the given destination
     * and going through its previous vertices until start is reached.
     * 
     * @param start object in vertex to start at
     * @param dest  object in vertex to end at
     * @return the list of previous vertices visited between dest and start
     */
    private List<T> shortestPath(T start, T dest) {
        final List<T> path = new ArrayList<T>();
        Vertex<T> v = getVertex(dest);

        if (v == null) {
            return path;
        }

        while (v != null && v.getElement() != start) {
            path.add(v.getElement());
            v = v.getPrev();
        }

        path.add(start); // Add start node to path
        Collections.reverse(path); // Get path from start -> dest

        return path;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Vertex<T> v : this.graph) {
            sb.append(v.getElement()).append(": ").append(v.getEdges()).append('\n');
        }
        return sb.toString();
    }

    // --------BEGINNING OF VERTEX CLASS--------------
    public class Vertex<T> {
        private T element;
        private Vertex<T> prev; // Null unless actively being used
        private LinkedList<Edge<T>> edges;

        /**
         * Constructor to create a Vertex object with the given element
         * 
         * @param data
         */
        public Vertex(T data) {
            this.element = data;
            this.prev = null;
            this.edges = new LinkedList<>();
        }

        /**
         * Associate a new neighbor with this vertex
         * 
         * @param destination the new neighbor
         * @param weight      the weight of this relationship
         * @return true upon edge creation, false otherwise
         */
        public boolean addEdge(T destination, int weight) {
            return edges.add(new Edge<T>(weight, this.element, destination));
        }

        /**
         * Delete all associated edges with this vertex. Only deletes outgoing edges,
         * incoming edges untouched
         * 
         * @return true upon deletion completion
         */
        public boolean deleteAllEdges() {
            this.edges.clear();
            return true;
        }

        /**
         * Delete a specific edge this vertex has
         * 
         * @param e edge to be deleted
         * @return true if any elements were removed, false otherwise
         */
        public boolean deleteEdge(Edge<T> e) {
            return this.edges
                    .removeIf(edge -> edge.getDestination() == e.getSource() && edge.getSource() == e.getDestination());
        }

        /**
         * 
         * @return List of all edges this vertex has
         */
        public LinkedList<Edge<T>> getEdges() {
            return this.edges;
        }

        /**
         * 
         * @return this vertex's element
         */
        public T getElement() {
            return element;
        }

        /**
         * set this vertex's element
         * 
         * @param element
         */
        public void setElement(T element) {
            this.element = element;
        }

        /**
         * 
         * @return this vertex's prev (if any)
         */
        public Vertex<T> getPrev() {
            return prev;
        }

        /**
         * set this vertex's prev
         * 
         * @param prev
         */
        public void setPrev(Vertex<T> prev) {
            this.prev = prev;
        }

        @Override
        public String toString() {
            return "Vertex [element=" + element + ", prev=" + prev + ", edges=" + edges + "]";
        }
    }
    // --------END OF VERTEX CLASS--------------

    // --------BEGINNING OF EDGE CLASS--------------
    public class Edge<T> {
        private int weight;
        private T source;
        private T destination;

        /**
         * Constructor to create a link between two objects
         * 
         * @param source
         * @param destination
         */
        public Edge(T source, T destination) {
            this(1, source, destination);
        }

        /**
         * Canonical onstructor to create a link between two objects with a given weight
         * 
         * @param weight
         * @param source
         * @param destination
         */
        public Edge(int weight, T source, T destination) {
            this.weight = weight;
            this.source = source;
            this.destination = destination;
        }

        /**
         * @return this edge's weight
         */
        public int getWeight() {
            return weight;
        }

        /**
         * set this edge's weight
         * 
         * @param weight
         */
        public void setWeight(int weight) {
            this.weight = weight;
        }

        /**
         * @return this edge's source
         */
        public T getSource() {
            return source;
        }

        /**
         * set this edge's source
         * 
         * @param source
         */
        public void setSource(T source) {
            this.source = source;
        }

        /**
         * @return this edge's destination
         */
        public T getDestination() {
            return destination;
        }

        /**
         * set this edge's destination
         * 
         * @param destination
         */
        public void setDestination(T destination) {
            this.destination = destination;
        }

        @Override
        public String toString() {
            return "Edge [weight=" + weight + ", source=" + source + ", destination=" + destination + "]";
        }
    }
    // --------END OF EDGE CLASS--------------

    // Graph testing
    public static void main(String[] args) {
        Graph<String> g = airports();
        System.out.println(g);
        System.out.println(g.BFS("ORD", "JFK"));
        System.out.println(g.dijkstra("ORD", "JFK"));
        System.out.println(g.dijkstra("ORD", "ORD"));
    }

    private static Graph<String> airports() {
        Graph<String> g = new Graph<>();

        g.insert("LAX");
        g.insert("SFO");
        g.insert("ORD");
        g.insert("DFW");
        g.insert("JFK");
        g.insert("MIA");
        g.insert("BOS");
        g.insertEdge("SFO", "JFK", 45);
        g.insertEdge("BOS", "JFK", 35);
        g.insertEdge("BOS", "MIA", 247);
        g.insertEdge("JFK", "MIA", 903);
        g.insertEdge("JFK", "DFW", 1387);
        g.insertEdge("MIA", "DFW", 523);
        g.insertEdge("MIA", "LAX", 411);
        g.insertEdge("DFW", "ORD", 335);
        g.insertEdge("DFW", "LAX", 49);
        g.insertEdge("ORD", "LAX", 120);
        return g;
    }

}
