/*
Author: Vincent Quintero
Email: 
Course: CSE 2010
Section: 02
Description of this file: Entity representing a game object at a specified tile
*/

public class Entity implements Comparable<Entity> {
    final char id;
    GameTile tile;

    /**
     * Constructor to create a new Entity
     * 
     * @param id
     * @param tile
     */
    public Entity(char id, GameTile tile) {
        this.id = id;
        this.tile = tile;
    }

    /**
     * @return this entity's id
     */
    public char getId() {
        return id;
    }

    /**
     * @return this entity's tile
     */
    public GameTile getTile() {
        return tile;
    }

    /**
     * set this entity's currently located tile
     * 
     * @param tile
     */
    public void setTile(GameTile tile) {
        this.tile = tile;
    }

    @Override
    public int compareTo(Entity o) {
        return String.valueOf(this.id).compareTo(String.valueOf(o.id));
    }

    @Override
    public String toString() {
        return "Entity [id=" + id + ", location=(" + tile.getY() + ", " + tile.getX() + ")]";
    }

}
