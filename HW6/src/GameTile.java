/*
Author: Vincent Quintero
Email: 
Course: CSE 2010
Section: 02
Description of this file: GameTile representing a character associated with a given position in the game
*/

// This class is dependent on the Game object's properties, perhaps worth nesting
public class GameTile {
    private char data;
    private int x, y;

    /**
     * Constructor to create a new GameTile
     * 
     * @param data
     * @param x
     * @param y
     */
    public GameTile(char data, int x, int y) {
        this.x = x;
        this.y = y;
        this.data = data;
    }

    /**
     * Determine if this tile holds something
     * 
     * @return true if this tile is not empty, false otherwise
     */
    public boolean isOccupied() {
        return data != Game.EMPTY_SPACE;
    }

    /**
     * Determine if this tile is an obstacle
     * 
     * @return true if this tile is an obstacle, false otherwise
     */
    public boolean isObstacle() {
        return data == Game.OBSTACLE;
    }

    /**
     * Determine if this tile is a creature
     * 
     * @return true if this tile is a creature, false otherwise
     */
    public boolean isCreature() {
        return Character.toString(data).matches(Game.CREATURE);
        // return Game.CREATURE_A <= (int) data && (int) data <= Game.CREATURE_Z;
    }

    /**
     * Determine if this tile is a player
     * 
     * @return true if this tile is a player, false otherwise
     */
    public boolean isPlayer() {
        return Character.toString(data).matches(Game.PLAYER);
    }

    /**
     * Determine if this tile is empty
     * 
     * @return true if this tile is empty, false otherwise
     */
    public boolean isEmpty() {
        return this.data == Game.EMPTY_SPACE;
    }

    /**
     * Determine if this tile is the objective
     * 
     * @return true if this tile is the objective, false otherwise
     */
    public boolean isObjective() {
        return this.data == Game.DESTINATION;
    }

    /**
     * 
     * @return this tile's data
     */
    public char getData() {
        return this.data;
    }

    /**
     * 
     * @return this tile's x position
     */
    public int getX() {
        return x;
    }

    /**
     * 
     * @return this tile's y position
     */
    public int getY() {
        return y;
    }

    /**
     * set this tile's data
     * 
     * @param data
     */
    public void setData(char data) {
        this.data = data;
    }

    /**
     * set this tile's x position
     * 
     * @param x
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * set this tile's y position
     * 
     * @param y
     */
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "" + data;
    }
}
