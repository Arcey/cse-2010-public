/*
Author: Vincent Quintero
Email: 
Course: CSE 2010
Section: 02
Description of this file: Logic to run the Humans vs Zombies game for Extra2's requirements
*/

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class HW6Extra2 {

    public static void main(final String[] args) throws FileNotFoundException {
        final Scanner input = new Scanner(System.in, "US-ASCII");
        final Game game = Game.loadGamestate(new File(args[0]), true);

        showBoard(game);

        boolean turnEnded = false;
        while (!turnEnded) {
            System.out.printf("%s %s, %s", PLAYER, game.getPlayer().getId(), PROMPT_MOVE);

            // Should only ever be one character followed by a newline
            turnEnded = game.movePlayer(Game.stringToMove(input.nextLine()));
        }
        System.out.println("");
        showBoard(game);

        // Move creatures
        if (game.status() == Game.GameState.PLAYING) {
            game.moveCreatures();
        }

        System.out.println("");
        postGame(game);
        input.close();
    }

    /**
     * Handle events after the game concludes
     * 
     * @param game
     *             the concluded game
     */
    private static void postGame(final Game game) {
        switch (game.status()) {
            case LOST:
                System.out.println(LOSE_MESSAGE);
                break;
            case WON:
                showBoard(game);
                System.out.println(WIN_MESSAGE);
                break;
            default:
                break;
        }
    }

    /**
     * Print the game board to the output screen
     * 
     * @param game
     *             to be printed
     */
    public static void showBoard(Game game) {
        System.out.println(game);
        System.out.println("");
    }

    private static final String PROMPT_MOVE = "please enter your move [u(p), d(own), l(eft), or r(ight)]: ";
    private static final String LOSE_MESSAGE = "One creature is not hungry any more!";
    private static final String WIN_MESSAGE = "Player 0 beats the hungry creatures!";
    private static final String PLAYER = "Player";
}
